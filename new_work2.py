
from GoogleOCR import GoogleOCR
import func
import os
import shutil
import zipfile


def convert_filee(src_file):
    # ------------------- copy and rename as '*/zip' --------------------------
    file_path, file_name = os.path.split(src_file)
    ret_name = os.path.join(file_path, file_name.replace('.docx', '.txt'))

    if os.path.isfile(ret_name):
        return None

    zip_file = os.path.join(file_path, 'temp.zip')
    shutil.copyfile(src_file, zip_file)

    # --------------------- unzip temp.zip file -------------------------------
    zip_folder = os.path.join(file_path, 'temp/')
    with zipfile.ZipFile(zip_file, 'r') as zip_ref:
        zip_ref.extractall(zip_folder)

    # -------------------- find images and OCR it -----------------------------
    img_folder = os.path.join(zip_folder, 'word', 'media')
    _, file_list_img, join_list_img = func.get_file_list(img_folder)

    ocr_text = ''
    csv_data = [src_file]
    for i in range(len(join_list_img)):
        img_json = class_ocr.get_json_google(join_list_img[i])

        if img_json is None:
            continue

        temp_text = img_json[0]['description']
        temp_text = temp_text.encode('UTF-8')

        ocr_text += temp_text + '\n'

        csv_data.append(file_list_img[i])
        csv_data.append(os.path.getsize(join_list_img[i]))

    func.save_text(ret_name, ocr_text)

    # ------------------ Remove temporary folders and files -------------------
    shutil.rmtree(zip_folder)
    os.remove(zip_file)

    return csv_data


def convert_folder(src_path):
    _, _, join_list_src = func.get_file_list(src_path)
    csv_data_path = os.path.join(src_path, 'image_list.csv')

    doc_list = []
    for i in range(len(join_list_src)):
        if join_list_src[i].endswith('Question.docx'):
            doc_list.append(join_list_src[i])

    for i in range(len(doc_list)):
        print str(i + 1) + '/' + str(len(doc_list)), doc_list[i]
        csv_data = convert_filee(doc_list[i])

        if csv_data is None:
            continue

        if os.path.isfile(csv_data_path):
            func.append_csv(csv_data_path, [csv_data])
        else:
            func.save_csv(csv_data_path, [csv_data])


if __name__ == '__main__':

    src_path = '../new_work4'

    class_ocr = GoogleOCR()

    convert_folder(src_path)
