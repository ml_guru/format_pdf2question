
import base64
import json
import requests
from datetime import datetime
import func


class GoogleOCR:

    def __init__(self):
        pass

    def __make_request_json(self, img_file, output_filename):

        # Read the image and convert to json
        with open(img_file, 'rb') as image_file:
            content_json_obj = {'content': base64.b64encode(image_file.read()).decode('UTF-8')}

        feature_json_arr = [{'type': 'TEXT_DETECTION'}, {'type': 'DOCUMENT_TEXT_DETECTION'}]
        request_list = {'features': feature_json_arr, 'image': content_json_obj}

        # Write the object to a file, as json
        with open(output_filename, 'w') as output_json:
            json.dump({'requests': [request_list]}, output_json)

    def __get_text_info(self, json_file):

        data = open(json_file, 'rb').read()
        try:
            response = requests.post(
                url='https://vision.googleapis.com/v1/images:annotate?key=AIzaSyCkNNeL7iPi5qsGbDJSAExcPOOqKFZD42Y',
                data=data,
                headers={'Content-Type': 'application/json'})

            ret_json = json.loads(response.text)
            ret_val = ret_json['responses'][0]

            if 'textAnnotations' in ret_val:
                return ret_val['textAnnotations']
            else:
                return None

        except Exception as e:
            return None

    def get_json_google(self, img_file):

        temp_json = 'temp.' + str(datetime.now().microsecond) + '.json'
        self.__make_request_json(img_file, temp_json)
        ret_json = self.__get_text_info(temp_json)
        func.rm_file(temp_json)

        return ret_json
