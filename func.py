import os
import cv2
import ast
import csv
import numpy as np


def load_csv(filename):
    if os.path.isfile(filename):
        file_csv = open(filename, 'rb')
        reader = csv.reader(file_csv)
        data_csv = []
        for row_data in reader:
            data_csv.append(row_data)

        file_csv.close()
        return data_csv
    else:
        return None


def save_csv(filename, data):
    file_out = open(filename, 'wb')
    writer = csv.writer(file_out)
    writer.writerows(data)
    file_out.close()


def append_csv(filename, data):
    file_out = open(filename, 'ab')
    writer = csv.writer(file_out)
    writer.writerows(data)
    file_out.close()


def load_text(filename):
    if os.path.isfile(filename):
        file_text = open(filename, 'r')
        text = file_text.read()
        file_text.close()
        return text
    else:
        return None


def save_text(filename, text):
    file_text = open(filename, 'w')
    file_text.write(text)
    file_text.close()


def create_folder(folder_name):
    if not os.path.isdir(folder_name):
        os.mkdir(folder_name)

    return folder_name


def find_most_common(lst):
    return max(set(lst), key=lst.count)


def get_file_list(root_dir):
    path_list = []
    file_list = []
    join_list = []

    for path, _, files in os.walk(root_dir):
        for name in files:
            path_list.append(path)
            file_list.append(name)
            join_list.append(os.path.join(path, name))

    return path_list, file_list, join_list


def dict_int(dict_parent, element):
    if element in dict_parent:
        return dict_parent[element]
    else:
        return 0


def get_rect_ocr_data_json(ocr_data, ind):
    p = ocr_data[ind]['boundingPoly']['vertices']
    rect = [dict_int(p[0], 'x'), dict_int(p[0], 'y'), dict_int(p[1], 'x'), dict_int(p[2], 'y')]

    return rect


def get_rect_ocr_data(ocr_data, ind):
    p = ocr_data[ind]
    rect = [int(p[0]), int(p[1]), int(p[2]), int(p[3])]

    return rect


def img_draw_ocr_all_rect(img, ocr_data, color=(0, 0, 255)):
    for j in range(1, len(ocr_data)):
        rect = get_rect_ocr_data(ocr_data, j)
        cv2.rectangle(img, (rect[0], rect[1]), (rect[2], rect[3]), color, 1)

    return img


def img_draw_ocr_rect(img, ocr_data, ind, color=(255, 0, 0)):
    rect = get_rect_ocr_data(ocr_data, ind)
    cv2.rectangle(img, (rect[0], rect[1]), (rect[2], rect[3]), color, 2)

    return img


def img_draw_rect(img, rect, color=(0, 255, 0), width=1):
    cv2.rectangle(img, (rect[0], rect[1]), (rect[2], rect[3]), color, width)

    return img


def check_overlap_ocr_data(ocr_data, rect_comp):
    for j in range(1, len(ocr_data)):
        rect = get_rect_ocr_data(ocr_data, j)
        if check_overlap_rect(rect, rect_comp):
            return True

    return False


def check_overlap_rect(rect1, rect2):
    min_x1 = min(rect1[0], rect1[2])
    max_x1 = max(rect1[0], rect1[2])
    min_y1 = min(rect1[1], rect1[3])
    max_y1 = max(rect1[1], rect1[3])

    min_x2 = min(rect2[0], rect2[2])
    max_x2 = max(rect2[0], rect2[2])
    min_y2 = min(rect2[1], rect2[3])
    max_y2 = max(rect2[1], rect2[3])

    if rect1 == rect2:
        return False
    elif max_x1 <= min_x2 or max_x2 <= min_x1 or max_y1 <= min_y2 or max_y2 <= min_y1:
        return False
    else:
        return True


def check_contain_rect(big_rect, small_rect):
    min_x1 = min(big_rect[0], big_rect[2])
    max_x1 = max(big_rect[0], big_rect[2])
    min_y1 = min(big_rect[1], big_rect[3])
    max_y1 = max(big_rect[1], big_rect[3])

    min_x2 = min(small_rect[0], small_rect[2])
    max_x2 = max(small_rect[0], small_rect[2])
    min_y2 = min(small_rect[1], small_rect[3])
    max_y2 = max(small_rect[1], small_rect[3])

    if min_x1 < min_x2 < max_x2 < max_x1 and min_y1 < min_y2 < max_y2 < max_y1:
        return True
    else:
        return False


def get_contain_counts(big_rect, ocr_data):
    cnt = 0
    for i in range(1, len(ocr_data)):
        sub_key_rect = get_rect_ocr_data(ocr_data, i)

        if check_contain_rect(big_rect, sub_key_rect):
            cnt += 1

    return cnt


def __reduce_rect_y2(rect):
    temp_rect = [rect[0], rect[1], rect[2], rect[3] - (rect[3] - rect[1]) / 9 - 1]
    return temp_rect


def get_overlap_counts(big_rect, ocr_data):
    cnt = 0
    temp1 = __reduce_rect_y2(big_rect)

    for i in range(1, len(ocr_data)):
        sub_key_rect = get_rect_ocr_data(ocr_data, i)
        temp2 = __reduce_rect_y2(sub_key_rect)

        if check_overlap_rect(temp1, temp2):
            cnt += 1

    return cnt


def anal_key(ocr_page_json, page_ind, key_type, key_ind, page_type, img, t_margin, l_margin, img_draw):
    page_height, page_width = img.shape[:2]

    key_rect = get_rect_ocr_data(ocr_page_json, key_ind)

    # ------------------- check top and left margin --------------------
    key_check_rect_above = [key_rect[0], key_rect[1] - t_margin, max(key_rect[0] + 20, key_rect[2] - 4), key_rect[1]-1]
    key_check_rect_left = [key_rect[0] - l_margin, key_rect[1], key_rect[0] - 1, key_rect[3] - 8]

    overlapped1 = check_overlap_ocr_data(ocr_page_json, key_check_rect_above)
    overlapped2 = check_overlap_ocr_data(ocr_page_json, key_check_rect_left)

    if img_draw:
        img = img_draw_rect(img, key_check_rect_above)
        img = img_draw_rect(img, key_check_rect_left)

    if not overlapped1 and not overlapped2:

        if page_type == 0:
            key_order = key_rect[1]
        else:
            if key_rect[0] < page_type:
                key_order = key_rect[1]
            else:
                key_order = int(page_height + key_rect[1])

        return [page_ind, key_order, key_type, key_rect]

    else:
        return None


def load_ocr_data(pdf_name, page_num, page_cnt):
    if len(str(page_cnt)) == 2:
        str_page_num = '{:02d}'.format(page_num)
    elif len(str(page_cnt)) == 3:
        str_page_num = '{:03d}'.format(page_num)
    elif len(str(page_cnt)) == 4:
        str_page_num = '{:04d}'.format(page_num)
    else:
        str_page_num = '{:05d}'.format(page_num)

    file_name = '../ocr/' + pdf_name + '/data_' + str_page_num + '.csv'

    if os.path.isfile(file_name):
        ocr_data = load_csv(file_name)
    else:
        ocr_data = None

    return ocr_data


def load_img_data(pdf_name, page_num, page_cnt):
    if len(str(page_cnt)) == 2:
        str_page_num = '{:02d}'.format(page_num)
    elif len(str(page_cnt)) == 3:
        str_page_num = '{:03d}'.format(page_num)
    elif len(str(page_cnt)) == 4:
        str_page_num = '{:04d}'.format(page_num)
    else:
        str_page_num = '{:05d}'.format(page_num)

    file_name = '../jpg/' + pdf_name + '/page-' + str_page_num + '.jpg'

    if os.path.isfile(file_name):
        img = cv2.imread(file_name)
    else:
        img = None

    return img


def check_key(ocr_data, key_pos, reg_exp, page_type, rect_half):
    key_rect = get_rect_ocr_data(ocr_data, key_pos)

    for i in range(len(reg_exp)):
        key_ind = key_pos - len(reg_exp) + i + 1
        if reg_exp[i] == 'n' and key_ind < 0:
            continue
        elif key_ind < 0:
            return False
        elif reg_exp[i] == 'n':
            rect1 = get_rect_ocr_data(ocr_data, key_ind)
            rect2 = get_rect_ocr_data(ocr_data, key_ind + 1)
            if rect1[0] > rect2[0]:
                continue
            elif page_type > 0 and rect1[2] < rect_half[0] < rect2[0]:
                continue
            # elif page_type > 0:
            #     continue
            elif page_type == 0 and rect2[0] - rect1[2] > 75:
                continue
            else:
                return False
        elif check_reg_exp(ocr_data[key_ind][4], reg_exp[i]):
            # ----------------- check words are one line -------------------
            key_rect_prev = get_rect_ocr_data(ocr_data, key_ind)
            if abs(key_rect[1] - key_rect_prev[1]) > 5:
                return False
            else:
                continue
        else:
            return False

    return True


def check_reg_exp(str_check, reg_exp):
    if str_check.isdigit() and str_check == str(int(str_check)) and reg_exp == 'd':
        if 0 <= int(str_check) < 100:
            return True
        else:
            return False
    elif str_check.isdigit() and str_check == str(int(str_check)) and reg_exp == 'D':
        if 0 <= int(str_check) < 400:
            return True
        else:
            return False
    elif reg_exp == 'T':
        if str_check[-1].isalpha() and str_check[:-1].isdigit():
            if 0 < int(str_check[:-1]) < 400 and str_check[-1].isupper():
                return True
            else:
                return False
        elif str_check.isdigit():
            if 0 < int(str_check) < 400:
                return True
            else:
                return False
        else:
            return False
    elif str_check.isalpha() and str_check.isupper() and reg_exp == 'U':    # 'CHAPTER'
        return True
    elif str_check.isalpha() and str_check[0].isupper() and reg_exp == 'u':     # 'Chapter'
        return True
    elif len(str_check) > 1 and not str_check[0].isdigit() and str_check[1:].isdigit() and reg_exp == 'S':  # 'd4'
        return True
    elif len(str_check) > 1 and str_check[:-1].isdigit() and str_check[-1].islower() and reg_exp == 's':    # '12a'
        return True
    elif (str_check == 'T' or str_check == 'O' or str_check == 'E' or str_check == 'Q' or str_check == 'U'
          or str_check == 'D' or str_check == '0' or str_check == '1' or str_check == '2' or str_check == 'WEB'
          or str_check == 'DATA') and reg_exp == 'B':  # 'Chapter'
        return True
    elif str_check.isalpha() and reg_exp == 'a':
        return True
    elif (str_check == 'Problem' or str_check == 'Exercise' or str_check == 'PROBLEM' or str_check == 'EXERCISE' or
          str_check == 'BRIEF' or str_check == 'Question') and reg_exp == 'p':
        return True
    elif str_check == 'Problems' and reg_exp == 'P':
        return True
    elif str_check == '.' and reg_exp == '.':
        return True
    elif str_check == ',' and reg_exp == ',':
        return True
    elif str_check == '-' and reg_exp == '-':
        return True
    elif str_check == '+' and reg_exp == '+':
        return True
    elif str_check == '*' and reg_exp == '*':
        return True
    elif str_check == '/' and reg_exp == '/':
        return True
    elif str_check == '(' and reg_exp == '(':
        return True
    elif str_check == ')' and reg_exp == ')':
        return True
    elif (str_check == 'A' or str_check == 'B' or str_check == 'C' or str_check == 'D') and reg_exp == 'A':
        return True
    elif str_check[:-1].isdigit() and str_check[-1] == 'A' and reg_exp == '@':
        return True
    elif str_check == '\xe2\x80\x93' and reg_exp == '%':
        return True
    elif str_check != 'Fig' and str_check != 'Figure' and str_check != 'Example' and reg_exp == 'n':
        return True
    else:
        return False


def check_prev_key_same_line(key_info_list, key_rect):
    # check previous start key is same line.
    if len(key_info_list) > 0:
        prev_key_type = key_info_list[-1][2]
        prev_key_rect = key_info_list[-1][4]
        prev_key_text = key_info_list[-1][3]

        if prev_key_type == 1 and abs(prev_key_rect[1] - key_rect[1]) < 28:
            return True
        elif prev_key_type == 0 and prev_key_text == 'Appendix' and abs(prev_key_rect[1] - key_rect[1]) < 35:
            return True

    return False


def check_ocr_re(reg_exp, ocr_page_json, chk_point, key_rect_y1=None):
    if key_rect_y1 is None:
        key_rect_y1 = get_rect_ocr_data(ocr_page_json, chk_point)[1]

    f_ok = True
    for j in range(len(reg_exp)):
        if chk_point + j >= len(ocr_page_json):
            f_ok = False
            break
        elif not check_reg_exp(ocr_page_json[chk_point + j][4], reg_exp[j]):
            f_ok = False
            break
        elif abs(key_rect_y1 - get_rect_ocr_data(ocr_page_json, chk_point + j)[1]) > 7:
            f_ok = False
            break

    return f_ok


def get_sub_rect_mix(mix_data, sub_rect):
    ocr_rect = []
    for i in range(1, len(mix_data)):
        if check_contain_rect(sub_rect, get_rect_ocr_data(mix_data, i)):
            ocr_rect.append(mix_data[i])

    return ocr_rect


def convert_mix2str(mix_data):
    ret_string = ''
    prev_rect = []
    for i in range(len(mix_data)):
        rect_i = get_rect_ocr_data(mix_data, i)
        if i == 0:
            if len(mix_data[0]) > 10:
                continue
            else:
                ret_string = mix_data[0][4]

        else:
            if abs(prev_rect[1] - rect_i[1]) < 10:
                if mix_data[i][4] == '.' or mix_data[i][4] == ',' or mix_data[i][4] == "'":
                    pass
                elif mix_data[i][4] == '(' or mix_data[i][4] == ')' or mix_data[i][4] == "?":
                    pass
                elif mix_data[i][4] == '*' or mix_data[i][4] == '+' or mix_data[i][4] == "-":
                    pass
                elif mix_data[i][4].isdigit() and mix_data[i - 1][4] == '.':
                    pass
                elif mix_data[i - 1][4] == '(' or mix_data[i - 1][4] == ')':
                    pass
                else:
                    ret_string += ' '
            else:
                ret_string += '\n'

            ret_string += mix_data[i][4]

        prev_rect = rect_i

    return ret_string


def check_columns(page_json, page_rect, get_min_value=False):
    page_width = page_rect[0] + page_rect[2]

    col_pos = range(80, 120)

    col_pos_int = []
    word_pos_left_cnt = []
    word_pos_center_cnt = []
    word_pos_right_cnt = []

    for i in range(len(col_pos)):
        x1 = int(page_width * (float(col_pos[i]) / 200 - 0.003))
        x2 = int(page_width * (float(col_pos[i]) / 200 + 0.003))
        col_pos_int.append([x1, x2])
        word_pos_left_cnt.append(0)
        word_pos_center_cnt.append(0)
        word_pos_right_cnt.append(0)

    for i in range(1, len(page_json)):
        sub_key_rect = get_rect_ocr_data(page_json, i)

        if sub_key_rect[1] < page_rect[1] or sub_key_rect[3] > page_rect[3]:
            continue

        for j in range(len(col_pos_int)):
            if sub_key_rect[2] < col_pos_int[j][0]:
                word_pos_left_cnt[j] += 1
            elif sub_key_rect[0] > col_pos_int[j][1]:
                word_pos_right_cnt[j] += 1
            else:
                word_pos_center_cnt[j] += 1

    min_ind = np.argmin(word_pos_center_cnt)
    min_val = min(word_pos_center_cnt)
    min_left = word_pos_left_cnt[min_ind]
    min_right = word_pos_right_cnt[min_ind]
    rect_words = word_pos_left_cnt[0] + word_pos_right_cnt[0] + word_pos_center_cnt[0]

    if get_min_value:
        return int(page_width * float(col_pos[min_ind]) / 200)

    if min_val > 7:
        return None
    elif 90 * min_val < rect_words:
        if min_left > 4 * min_right:
            return None
        elif min_right > 3.5 * min_left:
            return None
        else:
            return int(page_width * float(col_pos[min_ind]) / 200)
    else:
        return None


def key_big(key_rect, key_text, char_width):
    if type(key_text) == int:
        return float(key_rect[2] - key_rect[0]) / key_text / char_width
    else:
        return float(key_rect[2] - key_rect[0]) / len(key_text) / char_width


def rm_file(filename):
    if os.path.isfile(filename):
        os.remove(filename)
