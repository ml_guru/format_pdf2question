
from GoogleOCR import GoogleOCR
import func
import os


def conv_jpg2ocr_single(pdf_name, page):

    txt_path = '../ocr/' + pdf_name
    jpg_path = '../jpg/' + pdf_name
    filename = '/page-' + page + '.jpg'
    ocr_text = None

    if not os.path.isdir(txt_path):
        os.mkdir(txt_path)

    for i in range(10):
        ocr_text = class_ocr.get_json_google(jpg_path + filename)
        if ocr_text is not None:
            break

    if ocr_text is not None:

        ocr_page_data = []
        for i in range(len(ocr_text)):
            ocr_word_text = ocr_text[i]['description']
            ocr_word_data = func.get_rect_ocr_data_json(ocr_text, i)

            if i == 0:
                ocr_word_data.append('all')
            else:
                ocr_word_data.append(ocr_word_text.encode('utf8'))

            ocr_page_data.append(ocr_word_data)

        out_name = txt_path + '/' + filename.replace('page-', 'data_').replace('.jpg', '.csv')
        func.save_csv(out_name, ocr_page_data)

    print '\r'


class_ocr = GoogleOCR()

pdf_name = 'book115'
page_number = '615'
conv_jpg2ocr_single(pdf_name, page_number)
