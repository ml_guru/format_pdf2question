import func
import os


match_len_prev = 1


def correct_key_name(key_name):
    if key_name == 'Problems':
        return 'Problem'
    else:
        return key_name


def correction(list_sub_key_id, list_sub_key_y, list_sub_key_rect_id):
    global match_len_prev

    if len(list_sub_key_id) == 0:
        return [], [], []

    # ------------- adjust key length -----------------
    list_len = []
    if list_sub_key_id.__contains__(['1']) and list_sub_key_id.__contains__(['2']) and \
            list_sub_key_id.__contains__(['3']) and list_sub_key_id.__contains__(['4']) and match_len_prev == 1:
        if list_sub_key_id[0] == ['1', '-', '1']:
            match_len = 3
        else:
            match_len = 1
    else:
        len_cnt = [0, 0, 0, 0, 0, 0, 0]

        for key_ind in range(len(list_sub_key_id)):
            if list_sub_key_id[key_ind][-1] != ',':
                key_len = len(list_sub_key_id[key_ind])
                list_len.append(key_len)
                len_cnt[key_len] += 1.0/len(list_sub_key_id)

        if not list_len:
            return [], [], []

        match_len = func.find_most_common(list_len)

        for i in range(len(len_cnt)):
            rate = len_cnt[i] / max(len_cnt)
            if rate > 0.5 and i == match_len_prev:
                match_len = match_len_prev
                break

    # match_len = 3
    match_len_prev = match_len

    for key_ind in range(len(list_sub_key_id) - 1, -1, -1):
        if len(list_sub_key_id[key_ind]) != match_len:
            list_sub_key_id.pop(key_ind)
            list_sub_key_y.pop(key_ind)
            list_sub_key_rect_id.pop(key_ind)

    # print match_len

    # -------------- Remove incorrect number (2.001) --------------------
    for key_ind in range(len(list_sub_key_id) - 1, -1, -1):
        if match_len > 1 and list_sub_key_id[key_ind][-2] == '.' and list_sub_key_id[key_ind][-1][0] == '0':
            list_sub_key_id.pop(key_ind)
            list_sub_key_y.pop(key_ind)
            list_sub_key_rect_id.pop(key_ind)

    # ------------ adjust first keys ---------------------
    for key_ind in range(len(list_sub_key_id)):
        list_sub_key_id[key_ind][0] = correct_key_name(list_sub_key_id[key_ind][0])

    if match_len > 1:
        list_key_order = []
        for key_ind in range(len(list_sub_key_id)):
            list_key_order.append(list_sub_key_id[key_ind][0])

        if not list_key_order:
            return [], [], []

        match_key = func.find_most_common(list_key_order)
        # match_key = '16'

        for key_ind in range(len(list_sub_key_id) - 1, -1, -1):
            if list_sub_key_id[key_ind][0] != match_key:
                if list_sub_key_id[key_ind][0] == match_key[1:]:  # 14.4/4.5/14.6
                    list_sub_key_id[key_ind][0] = match_key
                else:
                    list_sub_key_id.pop(key_ind)
                    list_sub_key_y.pop(key_ind)
                    list_sub_key_rect_id.pop(key_ind)

    # ------------ adjust middle keys ---------------------
    for key_order in range(1, match_len - 1):
        list_key_order = []
        for key_ind in range(len(list_sub_key_id)):
            list_key_order.append(list_sub_key_id[key_ind][key_order])

        match_key = func.find_most_common(list_key_order)
        # print match_key

        for key_ind in range(len(list_sub_key_id) - 1, -1, -1):
            if list_sub_key_id[key_ind][key_order] != match_key:
                list_sub_key_id.pop(key_ind)
                list_sub_key_y.pop(key_ind)
                list_sub_key_rect_id.pop(key_ind)

    # -------------- adjust last key ---------------------
    keys = []
    for key_ind in range(len(list_sub_key_id)):
        if list_sub_key_id[key_ind][-1].isdigit():
            keys.append(int(list_sub_key_id[key_ind][-1]))
        else:
            keys.append(0)

    # print keys

    if len(list_sub_key_id) > 2 and keys[1] + 1 == keys[0] == keys[2] - 1 and \
            list_sub_key_y[1] <= list_sub_key_y[0] <= list_sub_key_y[2]:  # 2/1/3/4
        keys[0], keys[1] = keys[1], keys[0]
        list_sub_key_id[0], list_sub_key_id[1] = list_sub_key_id[1], list_sub_key_id[0]
        list_sub_key_y[0], list_sub_key_y[1] = list_sub_key_y[1], list_sub_key_y[0]
        list_sub_key_rect_id[0], list_sub_key_rect_id[1] = list_sub_key_rect_id[1], list_sub_key_rect_id[0]
    elif len(list_sub_key_id) > 3 and keys[1] + 1 == keys[2] == keys[0] - 1 == keys[3] - 2 and \
            list_sub_key_y[1] <= list_sub_key_y[2] <= list_sub_key_y[0] <= list_sub_key_y[3]:  # 3/1/2/4/5
        keys[0], keys[1], keys[2] = keys[1], keys[2], keys[0]
        list_sub_key_id[0], list_sub_key_id[1], list_sub_key_id[2] = \
            list_sub_key_id[1], list_sub_key_id[2], list_sub_key_id[0]
        list_sub_key_y[0], list_sub_key_y[1], list_sub_key_y[2] = \
            list_sub_key_y[1], list_sub_key_y[2], list_sub_key_y[0]
        list_sub_key_rect_id[0], list_sub_key_rect_id[1], list_sub_key_rect_id[2] = \
            list_sub_key_rect_id[1], list_sub_key_rect_id[2], list_sub_key_rect_id[0]

    f_error = True
    while f_error:
        f_error = False
        n = len(list_sub_key_id)
        for i in range(n):
            if not 0 < keys[i]:
                list_sub_key_id.pop(i), list_sub_key_y.pop(i), list_sub_key_rect_id.pop(i), keys.pop(i)
                f_error = True
                break
            # continue

            if 1 < i < n-1 and keys[i-2] + 2 == keys[i-1] + 1 == keys[i+1] != keys[i]:  # 2/(4)/3, 2/(2)/3, not 2/(3)/3
                list_sub_key_id.pop(i), list_sub_key_y.pop(i), list_sub_key_rect_id.pop(i), keys.pop(i)
                f_error = True
                break
            elif i > 0 and keys[i - 1] == keys[i]:  # 1/(1)
                list_sub_key_id.pop(i), list_sub_key_y.pop(i), list_sub_key_rect_id.pop(i), keys.pop(i)
                f_error = True
                break
            elif i > 1 and keys[i - 2] + 1 == keys[i - 1] and keys[i] == (keys[i - 1] + 1) * 10 + 1:  # 1/2/3/(41)/(51)
                keys[i] = (keys[i] - 1) / 10
                list_sub_key_id[i][-1] = str(keys[i])
                f_error = True
                break
            elif i < n - 1 and 10 < keys[i] == (keys[i + 1] - 1) * 10 + 1 and keys[i - 1] + 1 < keys[i + 1]:  # 3/(51)/6
                keys[i] = (keys[i] - 1) / 10
                list_sub_key_id[i][-1] = str(keys[i])
                f_error = True
                break
            elif i > 0 and keys[i] == (keys[i - 1] + 1) * 10:  # 1/2/3/(40)/(50)
                keys[i] /= 10
                list_sub_key_id[i][-1] = str(keys[i])
                f_error = True
                break
            elif 0 < i < n - 1 and keys[i] > 0 and keys[i] == (keys[i + 1] - 1) * 10 and keys[i - 1] < keys[i + 1]:  # 3/(50)/6
                keys[i] /= 10
                list_sub_key_id[i][-1] = str(keys[i])
                f_error = True
                break
            elif 1 < i < n - 1 and keys[i - 1] + 1 == keys[i + 1] - 1 and int('1' + str(keys[i - 1] + 1)) == keys[i]:  # 6/(17)/8
                keys[i] = int(str(keys[i])[1:])
                list_sub_key_id[i][-1] = str(keys[i])
                f_error = True
                break
            elif 1 < i < n - 1 and 10 < keys[i - 1] + 1 == keys[i + 1] - 1 and int(str(keys[i - 1])[-1]) + 1 == keys[i]:  # 11/(2)/13
                keys[i] = int('1' + str(keys[i]))
                list_sub_key_id[i][-1] = str(keys[i])
                f_error = True
                break
            elif 1 < i and keys[i - 2] + 1 == keys[i - 1] and keys[i - 1] + 11 < keys[i]:  # 25/26/(100) -----keys[i - 1] + 8 < keys[i]:
                list_sub_key_id.pop(i), list_sub_key_y.pop(i), list_sub_key_rect_id.pop(i), keys.pop(i)
                f_error = True
                break
            elif 1 < i < n-3 and keys[i-2] + 1 == keys[i-1] == keys[i+2] - 1 == keys[i+3] - 2:  # 14/15/(22)/15/16/17
                list_sub_key_id.pop(i), list_sub_key_y.pop(i), list_sub_key_rect_id.pop(i), keys.pop(i)
                f_error = True
                break
            elif 0 < i < n - 4 and keys[i - 1] + 1 == keys[i + 3] == keys[i + 4] - 1 != keys[i]:  # 1/(10)/11/12/2/3
                list_sub_key_id.pop(i), list_sub_key_y.pop(i), list_sub_key_rect_id.pop(i), keys.pop(i)
                f_error = True
                break
            elif 0 < i < n - 3 and keys[i - 1] + 1 == keys[i + 2] == keys[i + 3] - 1 != keys[i]:  # 1/(11)/12/2/3
                list_sub_key_id.pop(i), list_sub_key_y.pop(i), list_sub_key_rect_id.pop(i), keys.pop(i)
                f_error = True
                break
            elif 0 < i < n - 1 and keys[i - 1] + 1 == keys[i + 1] != keys[i]:  # 1/(12)/2
                list_sub_key_id.pop(i), list_sub_key_y.pop(i), list_sub_key_rect_id.pop(i), keys.pop(i)
                f_error = True
                break
            elif 0 < i < n - 2 and keys[i - 1] + 1 == keys[i] % 100 == keys[i + 1] - 1 and int(
                            keys[i] / 100) > 0:  # 43/(244)/45
                keys[i] %= 100
                list_sub_key_id[i][-1] = str(keys[i])
                f_error = True
                break
            elif i > 10 and keys[i] < int(i / 2):
                list_sub_key_id.pop(i), list_sub_key_y.pop(i), list_sub_key_rect_id.pop(i), keys.pop(i)
                f_error = True
                break

    # -------------- remove one key ----------------------
    if len(list_sub_key_id) == 1:
        return [], [], []

    return list_sub_key_id, list_sub_key_y, list_sub_key_rect_id


if __name__ == '__main__':
    # book_range = range(1, 31)
    book_range = [220]

    for book_ind in book_range:
        match_len_prev = 1
        book_name = 'book' + str(book_ind)

        if not os.path.isfile('temp_key/' + book_name + '_key.txt'):
            continue

        list_key_string_page = func.load_text('temp_key/' + book_name + '_key.txt').splitlines()

        save_book_key_id_list = ''

        for key_i in range(len(list_key_string_page)):
            # -------------- convert original format ------------
            print list_key_string_page[key_i]
            list_key_string = list_key_string_page[key_i].split('!')
            list_key_id = []
            list_key_y = []
            list_key_rect_id = []

            for j in range(len(list_key_string)):
                if list_key_string[j]:
                    list_key_id.append(list_key_string[j].split())
                    list_key_y.append(0)
                    list_key_rect_id.append(len(list_key_rect_id))

            # ------------------- correction ---------------------
            list_key_id, _, _ = correction(list_key_id, list_key_y, list_key_rect_id)

            # ------------------- save result -------------------
            str_key_id_list = ''
            for j in range(len(list_key_id)):
                str_key_id_list += ''.join(list_key_id[j]) + ' ! '

            print str_key_id_list, '\n'
            save_book_key_id_list += str_key_id_list + '\n'
            func.save_text('temp_key/ret_' + book_name + '_key.txt', save_book_key_id_list)
