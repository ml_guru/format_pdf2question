import os
import sys
import func
import cv2
import key_error_correction


class ExtractData:

    def __init__(self):
        self.key_ind = 0
        self.key_cur_page = 0
        self.key_next_page = 0
        self.key_cur_text = ''
        self.key_next_text = ''
        self.key_secondary = ''
        self.ret_root = '../result'
        self.ret_path = ''
        self.ocr_json_book = []
        self.list_sub_key_y = []
        self.list_sub_key_id = []
        self.list_sub_key_rect_id = []
        self.list_sub_key_page = []
        self.list_sub_key_rect = []
        self.primary_folder = ''
        self.secondary_folder = ''
        self.book_key_id_list = ''
        self.same_line_start = False
        self.same_line_end = False

    def inc_key(self):
        self.key_ind += 1
        return str(self.key_ind)

    def get_out_name(self):
        sub_folder = func.create_folder(self.primary_folder + '/' + self.secondary_folder)
        f_name = sub_folder + '/' + self.key_cur_text + '_' + self.secondary_folder + '_' + self.inc_key() + '.jpg'
        return f_name

    def output_result(self, img, ocr_mix, rect, img_name):
        # write image
        ret_img = img[rect[1]-2:rect[3]+2, rect[0]:rect[2]]
        cv2.imwrite(img_name, ret_img)

        # write text
        rect_extend = [rect[0], max(0, rect[1] - 5), rect[2], rect[3] + 8]
        ocr_rect = func.get_sub_rect_mix(ocr_mix, rect_extend)
        str_rect = func.convert_mix2str(ocr_rect)
        text_name = img_name.replace('.jpg', '.txt')
        func.save_text(text_name, str_rect)

        # write csv
        csv_file = self.primary_folder + '/' + self.primary_folder.split('/')[-1] + '.csv'
        if self.key_secondary == '':
            csv_data = [img_name.split('/')[-2], self.secondary_folder, img_name, text_name]
        else:
            csv_data = [self.key_secondary, self.secondary_folder, img_name, text_name]

        if not os.path.isfile(csv_file):
            csv_title = ['Sub_Key', 'Sub_folder', 'Image name', 'Text name']
            func.save_csv(csv_file, [csv_title])

        func.append_csv(csv_file, [csv_data])

    def draw_write_book_img(self, pdf_name, pdf_page, draw_rect, output_file, pdf_page_cnt):

        # ------------------ write the rect data image ---------------------
        ocr_page_img = func.load_img_data(pdf_name, pdf_page, pdf_page_cnt)
        ocr_mix = self.ocr_json_book[pdf_page]
        self.output_result(ocr_page_img, ocr_mix, draw_rect, output_file)

        # ----------------- check if need more extraction ------------------
        list_primary = ['ADDITIONAL PROBLEMS', 'ANALYTICAL QUESTIONS', 'Applied Questions',
                        'Brief Exercises', 'BRIEF EXERCISES',
                        'CHALLENGE PROBLEMS', 'Challenge Questions',
                        'CHAPTER EXERCISES', 'CHAPTER DISCUSSION QUESTIONS',
                        'COMPUTER EXERCISES', 'Conceptual Questions', 'Critical Thinking', 'CRITICAL THINKING',
                        'Drill Problems', 'DRILL PROBLEMS',
                        'DISCUSSION QUESTIONS', 'Discussion Questions', 'ESSAY QUESTIONS',
                        'EXERCISE', 'EXERCISES', 'Exercises', 'Exercise Set', 'Experimental Questions',
                        'Fill in the Blanks',
                        'Multiple Choice', 'MULTIPLE CHOICE', 'MULTIPLE - CHOICE QUESTIONS', 'SECTION REVIEW',
                        'Questions', 'QUESTIONS',
                        'QUANTITATIVE PROBLEMS', 'PRACTICE QUIZ',
                        'Practice Exercises', 'Practice Problems', 'PRACTICE PROBLEMS',
                        'Problems', 'PROBLEMS', 'Problem Set', 'PROBLEM SETS',
                        'Research Activities', 'REVIEW AND COMPREHENSION',
                        'Review Exercises', 'REVIEW PROBLEMS', 'Review Questions', 'REVIEW QUESTIONS',
                        'SUMMARY PRACTICE TEST', 'WORD PROBLEMS', 'UNDERSTANDING KEY CONCEPTS',
                        'SELF QUIZ', 'Solved Problems', 'Supplementary Problems', 'STUDY QUESTIONS',
                        'Word Problems', 'WRITE IT OUT']

        if self.key_cur_text == 'PROBLEM' and self.key_next_page - self.key_cur_page > 2:
            pass
        elif not list_primary.__contains__(self.key_cur_text):
            return

        ocr_rect = func.get_sub_rect_mix(ocr_mix, draw_rect)
        self.list_sub_key_page.append(pdf_page)
        self.list_sub_key_rect.append(draw_rect)

        # -------------- collect sub rect from big rect data ----------------
        reg_exp_list = ['U-d.d', 'd.d.d',
                        'd.d.',
                        'pd.d', 'pd-T', 'ppd-T', 'pA-T', 'pd',
                        'S.T', '*S.T',
                        'd.T', '*d.T', '*d.D', 'Bd.D', 'U.D',
                        'd-D', '*d-D', 'd-T', '*d-T', 'S-d', 'S-T',
                        'd/D', '*d/D', 'A/D', '*A/D',
                        'D', 'D,',
                        'D.',
                        'BD', 'Bd.',
                        '*D.', '**D.', '***D.',
                        'S', '*S'
                        '(d)'
                        ]

        word_rect = [0, 0, 0, 0]

        for i in range(len(ocr_rect)):
            # ---------- check x1 position of key ----------
            word_rect_prev = word_rect
            word_rect = func.get_rect_ocr_data(ocr_rect, i)
            if word_rect[0] - draw_rect[0] > 400: #450
                continue
            elif i > 0 and word_rect[0] > word_rect_prev[0] and abs(word_rect[1] - word_rect_prev[1]) < 10 and \
                    ocr_rect[i - 1][4] != self.key_cur_text:
                continue

            # -------------- check reg exp list ------------
            f_key = False
            str_key = []

            for j in range(len(reg_exp_list)):
                if func.check_ocr_re(reg_exp_list[j], ocr_rect, i):
                    str_key = []
                    for k in range(len(reg_exp_list[j])):
                        if reg_exp_list[j][k] == '*' or reg_exp_list[j][k] == 'B':
                            pass
                        elif reg_exp_list[j][k] == 'p' or reg_exp_list[j][k] == 'P':
                            pass
                        elif reg_exp_list[j][k] == '(' or reg_exp_list[j][k] == ')':
                            pass
                        elif reg_exp_list[j][k] == '.' and k + 1 == len(reg_exp_list[j]):  # case of 'd.'
                            pass
                        elif reg_exp_list[j][k] == 'T':
                            if ocr_rect[i + k][4][-1].isalpha():
                                str_key.append(ocr_rect[i + k][4][:-1])
                            else:
                                str_key.append(ocr_rect[i + k][4])
                        elif reg_exp_list[j][k] == '%':
                            str_key.append('-')
                        elif reg_exp_list[j][k] == 'S' and ocr_rect[i + k][4][0] != 'R':
                            str_key.append(ocr_rect[i + k][4][1:])
                        else:
                            if k > 0 and reg_exp_list[j][k] == 'd' and reg_exp_list[j][k - 1] == 'd':
                                str_key.append('.')

                            str_key.append(ocr_rect[i + k][4])

                    f_key = True
                    break

            # ------------- crop the rect data --------------
            if f_key:
                self.list_sub_key_y.append(word_rect[1])
                self.list_sub_key_id.append(str_key)
                self.list_sub_key_rect_id.append(len(self.list_sub_key_page) - 1)

    def get_out_name_sub_problem(self, str_key):
        str_key_new = str_key.replace('/', '-')
        file_path = func.create_folder(self.primary_folder + '/' + self.secondary_folder + '/' + str_key_new)
        file_ind = 0
        file_name = []

        while True:
            file_name = file_path + '/' + str_key_new + '_' + str(file_ind) + '.jpg'
            if os.path.isfile(file_name):
                file_ind += 1
            else:
                break

        return file_name

    def gen_sub_problems(self, pdf_name, pdf_page_cnt):

        # print self.primary_folder
        # print self.secondary_folder
        # print self.list_sub_key_id
        # print self.list_sub_key_y
        # print self.list_sub_key_rect_id
        # print self.list_sub_key_rect
        # print self.list_sub_key_page

        # -------------------------- write temp key list ---------------------------------
        str_key_id_list = ''
        for i in range(len(self.list_sub_key_id)):
            str_key_id_list += ' '.join(self.list_sub_key_id[i]) + '!'

        if str_key_id_list != '':
            self.book_key_id_list += str_key_id_list + '\n'
            func.save_text('temp_key/' + pdf_name + '_key.txt', self.book_key_id_list)

        # -------------------------- key list error correction ----------------------------
        self.list_sub_key_id, self.list_sub_key_y, self.list_sub_key_rect_id = \
            key_error_correction.correction(self.list_sub_key_id, self.list_sub_key_y, self.list_sub_key_rect_id)

        # -------------------------- Generate Sub Problem data ----------------------------
        key_page_ind = 0
        first_key = False
        str_key = ''

        for i in range(len(self.list_sub_key_id)):
            # --------- load page and key info ---------
            key_page_rect = self.list_sub_key_rect[self.list_sub_key_rect_id[i]]
            if key_page_ind != self.list_sub_key_page[self.list_sub_key_rect_id[i]]:
                key_page_ind = self.list_sub_key_page[self.list_sub_key_rect_id[i]]
                ocr_page_img = func.load_img_data(pdf_name, key_page_ind, pdf_page_cnt)
                ocr_mix = self.ocr_json_book[key_page_ind]

            # ------------- generate key ----------------
            str_key_prev = str_key
            str_key = ''
            for j in range(len(self.list_sub_key_id[i])):
                str_key += self.list_sub_key_id[i][j]

            # -------------- generate key rect -----------
            if first_key:
                if i > 0:
                    for j in range(self.list_sub_key_rect_id[i-1] + 1, self.list_sub_key_rect_id[i]):
                        key_page_ind_temp = self.list_sub_key_page[j]
                        ocr_page_img_temp = func.load_img_data(pdf_name, key_page_ind_temp, pdf_page_cnt)
                        ocr_mix_temp = self.ocr_json_book[key_page_ind_temp]
                        key_rect_temp = self.list_sub_key_rect[j]
                        self.output_result(ocr_page_img_temp, ocr_mix_temp, key_rect_temp,
                                           self.get_out_name_sub_problem(str_key_prev))

                key_y1 = key_page_rect[1]
                key_y2 = self.list_sub_key_y[i]
                key_rect = [key_page_rect[0], key_y1, key_page_rect[2], key_y2]
                if func.get_contain_counts(key_rect, ocr_mix) > 3:
                    self.output_result(ocr_page_img, ocr_mix, key_rect, self.get_out_name_sub_problem(str_key_prev))

            if i + 1 < len(self.list_sub_key_y):
                key_y1 = self.list_sub_key_y[i]
                if key_page_ind == self.list_sub_key_page[self.list_sub_key_rect_id[i + 1]] and \
                        key_page_rect == self.list_sub_key_rect[self.list_sub_key_rect_id[i + 1]]:
                    key_y2 = self.list_sub_key_y[i + 1]
                    first_key = False
                else:
                    key_y2 = key_page_rect[3]
                    first_key = True
                key_rect = [key_page_rect[0], key_y1, key_page_rect[2], key_y2]
                self.output_result(ocr_page_img, ocr_mix, key_rect, self.get_out_name_sub_problem(str_key))

            else:  # last key
                key_y1 = self.list_sub_key_y[i]
                key_y2 = key_page_rect[3]
                key_rect = [key_page_rect[0], key_y1, key_page_rect[2], key_y2]
                self.output_result(ocr_page_img, ocr_mix, key_rect, self.get_out_name_sub_problem(str_key))

                for j in range(self.list_sub_key_rect_id[i] + 1, len(self.list_sub_key_page)):
                    key_page_ind = self.list_sub_key_page[j]
                    ocr_page_img = func.load_img_data(pdf_name, key_page_ind, pdf_page_cnt)
                    key_page_rect = self.list_sub_key_rect[j]
                    ocr_mix = self.ocr_json_book[key_page_ind]
                    self.output_result(ocr_page_img, ocr_mix, key_page_rect, self.get_out_name_sub_problem(str_key))

        # ------------------------------- Format variables ------------------------------------
        self.list_sub_key_page = []
        self.list_sub_key_id = []
        self.list_sub_key_y = []
        self.list_sub_key_rect_id = []
        self.list_sub_key_rect = []

    def __extract_type0(self, pdf_name, key_para, json_list_out, book_info, pdf_page_cnt):
        [key_cur_page, key_next_page, _, _, key_cur_rect, key_next_rect] = key_para
        page_cur_rect = book_info[key_cur_page][0]
        page_height = book_info[key_cur_page][3]

        if key_next_rect == [0, 0, 0, 0]:
            key_next_rect[1] = page_cur_rect[3]

        if key_cur_page == key_next_page:

            key_rect = [page_cur_rect[0],
                        key_cur_rect[1] - 5,
                        page_cur_rect[2],
                        key_next_rect[1] - 3]

            ocr_page_json = json_list_out[key_cur_page]
            word_cnts = func.get_contain_counts(key_rect, ocr_page_json)
            if word_cnts < 5:
                return

            self.draw_write_book_img(pdf_name, key_cur_page, key_rect, self.get_out_name(), pdf_page_cnt)

        else:
            # ---------- make key rect list ----------
            key_rect_list = {key_cur_page: [page_cur_rect[0], key_cur_rect[1] - 5, page_cur_rect[2], page_cur_rect[3]]}

            for j in range(key_cur_page + 1, key_next_page):
                if j in book_info:
                    key_rect_list[j] = book_info[j][0]

            if key_next_page in book_info:
                page_next_rect = book_info[key_next_page][0]
                key_rect_list[key_next_page] = [page_next_rect[0], page_next_rect[1], page_next_rect[2],
                                                key_next_rect[1] - 5]

            # ------------ first page -----------------
            rect = key_rect_list[key_cur_page]
            self.draw_write_book_img(pdf_name, key_cur_page, rect, self.get_out_name(), pdf_page_cnt)

            # ------------ middle page ----------------
            for j in range(key_cur_page + 1, key_next_page):
                if j in book_info:
                    p_mid_rect = book_info[j][0]
                    p_mid_height = p_mid_rect[3] - p_mid_rect[1]
                    ocr_page_mid_json = json_list_out[j]

                    words = func.get_contain_counts(p_mid_rect, ocr_page_mid_json)

                    if words < 10:
                        return
                    elif float(p_mid_height) / words > 40:
                        return
                    elif p_mid_rect[1] > int(page_height / 4) and p_mid_height < page_height / 10:
                        return
                    elif p_mid_height < page_height / 50:
                        return

                    self.draw_write_book_img(pdf_name, j, p_mid_rect, self.get_out_name(), pdf_page_cnt)

                else:
                    return

            # -------------- last page ------------------
            page_next_rect = book_info[key_next_page][0]

            ocr_page_json = json_list_out[key_next_page]
            words = func.get_contain_counts(key_rect_list[key_next_page], ocr_page_json)

            if words < 5:
                return
            elif float(key_next_rect[1] - 5 - page_next_rect[1]) / words > 7.5:
                return
            else:
                rect = key_rect_list[key_next_page]
                self.draw_write_book_img(pdf_name, key_next_page, rect, self.get_out_name(), pdf_page_cnt)

    def __extract_type1(self, pdf_name, key_para, book_info, pdf_page_cnt, page_type_list, key_type):

        [key_cur_page, key_next_page, key_cur_pos, key_next_pos, key_cur_rect, key_next_rect] = key_para

        page_cur_rect = book_info[key_cur_page][0]
        if key_next_rect == [0, 0, 0, 0]:
            key_next_rect[1] = page_cur_rect[3]

        if key_cur_page == key_next_page:
            col_pos = page_type_list[key_cur_page]
            # -------- in case of start and end is same page ---------
            if key_cur_pos == key_next_pos == 0:
                key_rect = [book_info[key_cur_page][0][0],
                            key_cur_rect[1] - 5,
                            col_pos,
                            key_next_rect[1] - 5]

                if func.get_contain_counts(key_rect, self.ocr_json_book[key_cur_page]) < 5:
                    return

                self.draw_write_book_img(pdf_name, key_cur_page, key_rect, self.get_out_name(), pdf_page_cnt)

                if self.same_line_start and self.same_line_end:
                    key_rect2 = [col_pos,
                                 key_cur_rect[1] - 5,
                                 book_info[key_next_page][0][2],
                                 key_next_rect[1] - 5]
                    self.draw_write_book_img(pdf_name, key_next_page, key_rect2, self.get_out_name(), pdf_page_cnt)

            elif key_cur_pos == key_next_pos == 1:
                key_rect = [col_pos,
                            key_cur_rect[1] - 5,
                            book_info[key_cur_page][0][2],
                            key_next_rect[1] - 5]
                if func.get_contain_counts(key_rect, self.ocr_json_book[key_cur_page]) < 5:
                    return
                self.draw_write_book_img(pdf_name, key_cur_page, key_rect, self.get_out_name(), pdf_page_cnt)

            else:
                key_rect1 = [book_info[key_cur_page][0][0],
                             key_cur_rect[1] - 5,
                             col_pos,
                             book_info[key_cur_page][0][3]]
                self.draw_write_book_img(pdf_name, key_cur_page, key_rect1, self.get_out_name(), pdf_page_cnt)

                if self.same_line_start:
                    pos_y1 = key_cur_rect[1] - 5
                else:
                    pos_y1 = book_info[key_cur_page][0][1]

                key_rect2 = [col_pos,
                             pos_y1,
                             book_info[key_cur_page][0][2],
                             key_next_rect[1] - 5]

                if func.get_contain_counts(key_rect2, self.ocr_json_book[key_cur_page]) < 2:
                    return

                self.draw_write_book_img(pdf_name, key_cur_page, key_rect2, self.get_out_name(), pdf_page_cnt)

        # ---------------------------- in case of start and end is different page -----------------------------
        else:
            # ------- first page---------
            if book_info[key_cur_page][0][3] - book_info[key_cur_page][2] > 100:
                y2 = book_info[key_cur_page][0][3]
            else:
                y2 = book_info[key_cur_page][2]

            col_pos = page_type_list[key_cur_page]
            if col_pos == 0:
                temp_rect = [book_info[key_cur_page][0][0],
                             key_cur_rect[1] - 5,
                             book_info[key_cur_page][0][2],
                             y2]

                col_pos = func.check_columns(self.ocr_json_book[key_cur_page], temp_rect)

                if col_pos is None:
                    col_pos = int((book_info[key_cur_page][0][0] + book_info[key_cur_page][0][2]) / 2)

                y1 = key_cur_rect[1] - 5
            else:
                # if key_type == 4:
                if self.same_line_start:
                    y1 = key_cur_rect[1] - 5
                else:
                    y1 = book_info[key_cur_page][1]

            if key_cur_pos == 0:
                key_rect1 = [book_info[key_cur_page][0][0],
                             key_cur_rect[1] - 5,
                             col_pos,
                             y2]

                key_rect2 = [col_pos,
                             y1,
                             book_info[key_cur_page][0][2],
                             y2]

                self.draw_write_book_img(pdf_name, key_cur_page, key_rect1, self.get_out_name(), pdf_page_cnt)
                self.draw_write_book_img(pdf_name, key_cur_page, key_rect2, self.get_out_name(), pdf_page_cnt)
            else:
                key_rect = [col_pos,
                            key_cur_rect[1] - 5,
                            book_info[key_cur_page][0][2],
                            y2]
                self.draw_write_book_img(pdf_name, key_cur_page, key_rect, self.get_out_name(), pdf_page_cnt)

            # ------------ mid page --------------
            for j in range(key_cur_page + 1, key_next_page):
                if j in book_info:
                    if book_info[j][0][3] - book_info[j][1] < 100 and book_info[j][1] > 1000:
                        return
                    elif func.get_contain_counts(book_info[j][0], self.ocr_json_book[j]) < 5:
                        return

                    col_pos = page_type_list[j]
                    if col_pos == 0:
                        if j < key_next_page:
                            col_pos = int((book_info[j][0][0] + book_info[j][0][2]) / 2)
                        else:
                            return

                    key_rect1 = [book_info[j][0][0],
                                 book_info[j][1],
                                 col_pos,
                                 book_info[j][2]]

                    key_rect2 = [col_pos,
                                 book_info[j][1],
                                 book_info[j][0][2],
                                 book_info[j][2]]

                    self.draw_write_book_img(pdf_name, j, key_rect1, self.get_out_name(), pdf_page_cnt)
                    self.draw_write_book_img(pdf_name, j, key_rect2, self.get_out_name(), pdf_page_cnt)
                else:
                    return

            # ------------- last page -------------
            col_pos = page_type_list[key_next_page]
            if col_pos == 0:
                col_pos = int((book_info[key_next_page][0][0] + book_info[key_next_page][0][2]) / 2)

            if key_next_pos == 0:
                if key_next_rect[1] - book_info[key_next_page][0][1] > 20:
                    key_rect1 = [book_info[key_next_page][0][0],
                                 book_info[key_next_page][1],
                                 col_pos,
                                 key_next_rect[1] - 5]

                    if func.get_contain_counts(key_rect1, self.ocr_json_book[key_next_page]) < 5:
                        return

                    self.draw_write_book_img(pdf_name, key_next_page, key_rect1, self.get_out_name(), pdf_page_cnt)

                    if self.same_line_end:
                        key_rect2 = [col_pos,
                                     book_info[key_next_page][1],
                                     book_info[key_next_page][0][2],
                                     key_next_rect[1] - 5]
                        self.draw_write_book_img(pdf_name, key_next_page, key_rect2, self.get_out_name(),
                                                 pdf_page_cnt)

            else:
                key_rect1 = [book_info[key_next_page][0][0],
                             book_info[key_next_page][1],
                             col_pos,
                             book_info[key_next_page][2]]

                self.draw_write_book_img(pdf_name, key_next_page, key_rect1, self.get_out_name(), pdf_page_cnt)

                if key_next_rect[1] - book_info[key_next_page][0][1] > 20:
                    key_rect2 = [col_pos,
                                 book_info[key_next_page][1],
                                 book_info[key_next_page][0][2],
                                 key_next_rect[1] - 5]

                    if func.get_contain_counts(key_rect2, self.ocr_json_book[key_next_page]) < 5:
                        return

                    self.draw_write_book_img(pdf_name, key_next_page, key_rect2, self.get_out_name(), pdf_page_cnt)

    def extract_data(self, pdf_name, json_list_out, pdf_info, pdf_page_cnt, ocr_json_list, page_range, same_line_start, same_line_end):

        key_info_list, book_info, page_type_list = pdf_info
        self.ocr_json_book = ocr_json_list

        _ = func.create_folder(self.ret_root)
        self.ret_path = func.create_folder(self.ret_root + '/' + pdf_name)
        self.book_key_id_list = ''

        self.same_line_start = same_line_start
        self.same_line_end = same_line_end

        key_error_correction.match_len_prev = 1

        for i in range(len(key_info_list)):

            sys.stdout.write('\rExtracting key: %d/%d' % (i + 1, len(key_info_list)))
            sys.stdout.flush()

            key_type = key_info_list[i][2]

            if key_type == 1 or key_type == 4:
                self.key_ind = 0

                # =============================== Step 1. Get key informations ==============================
                self.key_cur_page = key_info_list[i][0]
                key_cur_rect = key_info_list[i][4]
                self.key_cur_text = key_info_list[i][3]
                self.key_secondary = key_info_list[i][5]

                key_len = 1
                while True:
                    if i + key_len < len(key_info_list):
                        self.key_next_page = key_info_list[i + key_len][0]
                        key_next_rect = key_info_list[i + key_len][4]
                        self.key_next_text = key_info_list[i + key_len][3]

                        if same_line_start and \
                                self.key_next_page == self.key_cur_page and key_next_rect[1] < key_cur_rect[1]:
                            key_len += 1
                        else:
                            break

                    else:
                        self.key_next_page = page_range[-1]
                        key_next_rect = [0, 0, 0, 0]
                        self.key_next_text = 'end'
                        break

                cnt1 = 0
                cnt_total = 0
                for j in range(self.key_cur_page, self.key_next_page + 1):
                    cnt_total += 1
                    if j in page_type_list:
                        if page_type_list[j] > 0:
                            cnt1 += 1

                if cnt1 * 3 >= cnt_total:
                    page_type = 1
                else:
                    page_type = 0

                if page_type == 0:
                    key_cur_pos = 0
                    key_next_pos = 0
                else:
                    if key_info_list[i][1] == key_info_list[i][4][1]:
                        key_cur_pos = 0
                    else:
                        key_cur_pos = 1

                    if i < len(key_info_list) - 1:
                        if key_info_list[i + key_len][1] == key_info_list[i + key_len][4][1]:
                            key_next_pos = 0
                        else:
                            key_next_pos = 1
                    else:
                        key_next_pos = 0

                # ================================ Step 2. Extract the data =================================
                key_para = [self.key_cur_page, self.key_next_page, key_cur_pos, key_next_pos, key_cur_rect, key_next_rect]
                cat_key = self.key_cur_text[0].upper() + self.key_cur_text[1:].lower()
                if cat_key[-1] == 's':
                    cat_key = cat_key[:-1]

                if cat_key == 'Palco':
                    print i, cat_key
                self.primary_folder = func.create_folder(self.ret_path + '/' + cat_key)

                if self.key_secondary == '':
                    self.secondary_folder = 'p' + str(self.key_cur_page) + '_' + str(i)
                else:
                    self.secondary_folder = self.key_secondary + '_p' + str(self.key_cur_page) + '_' + str(i)

                if page_type == 0:
                    self.__extract_type0(pdf_name, key_para, json_list_out, book_info, pdf_page_cnt)
                else:
                    self.__extract_type1(pdf_name, key_para, book_info, pdf_page_cnt, page_type_list, key_type)

                # ================================ Step 3. Save Sub-Problems ================================
                self.gen_sub_problems(pdf_name, pdf_page_cnt)

        print '\r'
