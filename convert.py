import pdf2jpg
import os
import sys
import func
from GoogleOCR import GoogleOCR
from Extract_data import ExtractData
import cv2
import thread
import time


def load_config():
    global list_key_words_start, list_key_words_end, list_key_ignore_start, list_key_ignore_end
    global list_key_words_end_ignore, list_key_remove, list_en_words, list_key_stop
    list_key_words_start = func.load_text('config/key_words_start.txt').splitlines()
    list_key_words_end = func.load_text('config/key_words_end.txt').splitlines()
    list_key_ignore_start = func.load_text('config/key_ignore_start.txt').splitlines()
    list_key_ignore_end = func.load_text('config/key_ignore_end.txt').splitlines()
    list_key_words_end_ignore = func.load_text('config/key_words_end_ignore.txt').splitlines()
    list_key_remove = func.load_text('config/key_remove.txt').splitlines()
    list_en_words = func.load_text('config/wordsEN.txt').splitlines()
    list_key_stop = func.load_text('config/key_stop.txt').splitlines()


def conv_pdf2jpg(pdf_name):
    print "Converting %s to jpg ..." % format(pdf_name + '.pdf')
    jpg_path = '../jpg/' + pdf_name
    if not os.path.isdir(jpg_path):
        os.mkdir(jpg_path)

    pdf2jpg.pdf2jpg_ppm('../pdf/' + pdf_name + '.pdf', jpg_path + '/page', page=0)


def conv_jpg2ocr(pdf_name, f_thread=False):
    global thread_state

    _, file_list, join_list = func.get_file_list('../jpg/' + pdf_name)

    txt_path = '../ocr/' + pdf_name
    if not os.path.isdir(txt_path):
        os.mkdir(txt_path)

    # for i in range(150, 180):
    for i in range(len(join_list)):
        sys.stdout.write('\rOCR page: %d/%d' % (i + 1, len(join_list)))
        sys.stdout.flush()

        if f_thread:
            f_run_thread = False

            while not f_run_thread:
                for j in range(len(thread_state)):
                    if thread_state[j] == 0:
                        thread_state[j] = 1
                        thread.start_new_thread(__ocr_img, ('thread' + str(j), txt_path, join_list, file_list, i, j))
                        f_run_thread = True
                        break

                time.sleep(0.05)

        else:
            __ocr_img('', txt_path, join_list, file_list, i, 0)

    if f_thread:
        time2 = time.time()
        while True:
            # print sum(thread_state)
            if sum(thread_state) == 0:
                break
            else:
                time.sleep(0.5)
                if time.time() - time2 > 15:
                    break

    print '\r'


def __ocr_img(thread_name, txt_path, join_list, file_list, index, thread_ind):
    global thread_state

    ocr_text = None
    for i in range(10):
        ocr_text = class_ocr.get_json_google(join_list[index])
        if ocr_text is not None:
            break

    if ocr_text is not None:

        ocr_page_data = []
        for i in range(len(ocr_text)):
            ocr_word_text = ocr_text[i]['description']
            ocr_word_data = func.get_rect_ocr_data_json(ocr_text, i)

            if i == 0:
                ocr_word_data.append('all')
            else:
                ocr_word_data.append(ocr_word_text.encode('utf8'))

            ocr_page_data.append(ocr_word_data)

        out_name = txt_path + '/' + file_list[index].replace('page-', 'data_').replace('.jpg', '.csv')
        func.save_csv(out_name, ocr_page_data)

    thread_state[thread_ind] = 0


def get_pdf_type(ocr_json_list):
    total_both = 0
    page_col_list = {}
    for page_ind in ocr_json_list:
        page_json = ocr_json_list[page_ind]

        page_rect = func.get_rect_ocr_data(page_json, 0)
        page_col_list[page_ind] = func.check_columns(page_json, page_rect)

        if page_col_list[page_ind] is not None:
            total_both += 1

    # print total_both, len(ocr_json_list)
    if int(total_both * 5) < len(ocr_json_list):
        return 0, page_col_list
    elif int(total_both * 2) < len(ocr_json_list):
        return 2, page_col_list
    else:
        return 1, page_col_list


def get_page_info(page_img, page_json, page_type, page_ind, remove_header, remove_footer, px1, py1, px2, py2):
    page_height, page_width = page_img.shape[:2]

    # Page rect
    page_rect = func.get_rect_ocr_data(page_json, 0)

    page_rect[1] += remove_header
    page_rect[3] -= remove_footer

    page_rect[0] -= int(page_width / 80)
    page_rect[1] -= int(page_width / 80)
    page_rect[2] += int(page_width / 80)
    page_rect[3] += int(page_height / 80)

    page_rect[0] = max(page_rect[0], 0)
    page_rect[1] = max(page_rect[1], 0)
    page_rect[2] = min(page_rect[2], page_width - 1)
    page_rect[3] = min(page_rect[3], page_height - 1)

    if page_ind >= 0 and px1 is not None:
        page_rect[0] = px1

    if py1 is not None:
        page_rect[1] = py1

    if px2 is not None:
        page_rect[2] = px2

    if py2 is not None:
        page_rect[3] = py2

    # if page_ind % 2 == 1:
    #     page_rect[1] = 200
    # else:
    #     page_rect[1] = 110

    # header and footer paragraph
    max_cross_min = page_rect[1]
    max_cross_max = page_rect[3]
    rect_half = None

    if page_type > 0:
        rect_half = [page_type - 2, page_rect[1], page_type + 2, page_rect[3]]
        cross_half_list = [page_rect[1]]

        for j in range(1, len(page_json)):
            rect = func.get_rect_ocr_data(page_json, j)
            if func.check_overlap_rect(rect, rect_half) and page_json[j][4].isalpha():
                if float(page_height) * 0.2 < float(rect[1]) < float(page_height) * 0.75:
                    continue

                cross_half_list.append(rect[1])
                cross_half_list.append(rect[3])

        cross_half_list.append(page_rect[3])
        cross_half_list.sort()

        max_dist = 0

        if len(cross_half_list) == 4 and cross_half_list[2] - cross_half_list[1] < 20:
            pass

        else:
            for j in range(len(cross_half_list) - 1):
                dist = cross_half_list[j + 1] - cross_half_list[j]
                if dist > max_dist:
                    max_dist = dist
                    # max_cross_min = cross_half_list[j]
                    # max_cross_max = cross_half_list[j + 1] - 4

    return page_rect, max_cross_min, max_cross_max, rect_half, page_height, page_width


def detect_secondary_key(ocr_page_json, chk_ind):
    sec_key = ''

    reg_exp_list = ['d.d.d', 'd-d-d', 'd.d-d', 'A.d-d', 'A-d-d',
                    'A.d', 'd.d', 'd.s', 'd-d', 'd-T', 'd%d', 'd+d',
                    'dd', 'd']
    sec_key_ignore = ['+', ',', '_', 'RI', 'R111', 'X', '%']
    key_rect_y1 = func.get_rect_ocr_data(ocr_page_json, chk_ind - 1)[1]

    for i in range(len(reg_exp_list)):
        if func.check_ocr_re(reg_exp_list[i], ocr_page_json, chk_ind, key_rect_y1):
            if chk_ind + len(reg_exp_list[i]) < len(ocr_page_json):
                str1 = ocr_page_json[chk_ind + len(reg_exp_list[i])][4]
            else:
                str1 = ''

            if chk_ind + len(reg_exp_list[i]) + 1 < len(ocr_page_json):
                str2 = ocr_page_json[chk_ind + len(reg_exp_list[i]) + 1][4]
            else:
                str2 = ''

            # if str1.islower() and str1 != 'a' and \
            #         not (len(str1) == 1 and (str2 == '.' or str2 == '(' or str2.isdigit())):
            #     return 'None'

            if reg_exp_list[i] == 'dd' and str1.isdigit():
                return ''
            elif reg_exp_list[i] == 'd' and str2.isdigit() and str1 != '$' and str1 != 'In' and str1 != 'Figure' and \
                    str1 != 'Table' and str1 != 'A' and str1 != 'Find' and str1 != 'Evaluate':
                return ''
            elif reg_exp_list[i] == 'd' and str1.isdigit():
                return ''
            elif sec_key_ignore.__contains__(str1):
                if reg_exp_list[i] == 'd.d' and str1 == ',' and len(str2) > 0 and str2[0].isupper():
                    pass
                else:
                    return ''
            elif str1 != 'Intelligence' and str2 == 'Quotient':
                return ''

            for j in range(len(reg_exp_list[i])):
                if reg_exp_list[i][j] == 'd' or reg_exp_list[i][j] == 'T':
                    if reg_exp_list[i] == 'dd' and j == 1:  # case of 'dd'
                        sec_key += '-'
                    sec_key += ocr_page_json[chk_ind + j][4]
                elif reg_exp_list[i][j] == 's' or reg_exp_list[i][j] == 'A':
                    sec_key += ocr_page_json[chk_ind + j][4]
                elif reg_exp_list[i][j] == '%':
                    sec_key += '-'
                elif reg_exp_list[i][j] == '+':
                    sec_key += '-'
                else:
                    sec_key += reg_exp_list[i][j]

            # print '\r', sec_key, str1, str2
            return sec_key

    # ----------------------- ignore when case of 'Chapter ...' -----------------------
    stop_reg_exp_list = ['...']

    for i in range(len(stop_reg_exp_list)):
        if func.check_ocr_re(stop_reg_exp_list[i], ocr_page_json, chk_ind, key_rect_y1):
            return 'None'

    # ------------------ case of next text is 3 digit '142' -> '14-2' ------------------------
    if chk_ind < len(ocr_page_json):
        if ocr_page_json[chk_ind][4].isdigit() and len(ocr_page_json[chk_ind][4]) == 3:

            if len(ocr_page_json) > chk_ind + 1:
                str1 = ocr_page_json[chk_ind + 1][4]
            else:
                str1 = ''

            if len(ocr_page_json) > chk_ind + 2:
                str2 = ocr_page_json[chk_ind + 2][4]
            else:
                str2 = ''

            if len(ocr_page_json) > chk_ind + 1 and str1.islower():
                return 'None'

            sec_key = ocr_page_json[chk_ind][4][0:2] + '-' + ocr_page_json[chk_ind][4][2]
            if sec_key_ignore.__contains__(str1):
                return ''
            elif str1.isdigit() and str2 == ')':
                return ''
            elif str1 == '.' and str2.isdigit():
                return ''

            # print '\r', sec_key, str1, str2
            return sec_key
        else:
            return ''
    else:
        return ''


def check_start_key(ocr_page_json, key_ind, char_width, page_type, page_rect, key_len, page_half):
    global primary_key, secondary_key

    primary_key = ocr_page_json[key_ind][4]
    primary_key_rect = [func.get_rect_ocr_data(ocr_page_json, key_ind)]
    primary_key_width = primary_key_rect[0][2] - primary_key_rect[0][0]

    for i in range(1, key_len):
        if key_ind + i < len(ocr_page_json):
            primary_key += (' ' + ocr_page_json[key_ind + i][4])
            primary_key_rect.append(func.get_rect_ocr_data(ocr_page_json, key_ind + i))
            primary_key_width += (primary_key_rect[-1][2] - primary_key_rect[-1][0])
        else:
            return False

    # check key_start_list
    if not list_key_words_start.__contains__(primary_key):
        return False

    if key_ind + key_len < len(ocr_page_json):
        # key_text_next = ocr_page_json[key_ind + key_len][4]
        key_rect_next = func.get_rect_ocr_data(ocr_page_json, key_ind + key_len)
    else:
        return False

    # check key position x1
    if page_type == 0:
        if primary_key == 'PROBLEMS' and page_rect[2] - primary_key_rect[0][2] < 70:
            pass
        elif primary_key == 'EXAMPLE' and page_rect[2] - primary_key_rect[0][2] < 100:
            pass
        # elif primary_key_rect[0][0] > int((page_rect[0] + page_rect[2]) / 2.2):
        #     return False
        # elif primary_key_rect[0][0] < int((page_rect[0] + page_rect[2]) / 2) < primary_key_rect[0][2]:
        #     return False
    else:
        if primary_key == 'Problems' or primary_key == 'PROBLEMS':
            page_width_div = int((page_rect[2] - page_rect[0]) / 4)
        else:
            page_width_div = int((page_rect[2] - page_rect[0]) / 6)

        if page_rect[0] + page_width_div * 1.5 < primary_key_rect[0][0] < page_half[0]:
            return False
        elif max(page_half[0], int((page_rect[0] + page_rect[2])/2)) + page_width_div + 10 < \
                primary_key_rect[0][0] < page_rect[2]:
            return False

    # detect secondary key
    secondary_key = detect_secondary_key(ocr_page_json, key_ind + key_len)
    # print '\r', secondary_key
    if secondary_key == 'None':
        return False

    if secondary_key != '':
        if primary_key_rect[-1][2] + 40 < key_rect_next[0] or key_rect_next[0] < primary_key_rect[-1][2]:
            secondary_key = ''

    if key_ind + key_len == len(ocr_page_json):
        return True

    if float(primary_key_width) / len(primary_key.replace(' ', '')) > char_width * 1.4:
        return True

    if page_type == 0:

        if primary_key == 'Exercise':  # check after 4~10 words are same line with key
            f_stop = False
            for k in range(3, 9):
                if key_ind + key_len + k < len(ocr_page_json):
                    key_rect_next1 = func.get_rect_ocr_data(ocr_page_json, key_ind + key_len + k)
                    if abs(primary_key_rect[-1][1] - key_rect_next1[1]) < 10:
                        f_stop = True
                        break

            if f_stop:
                return False

        elif primary_key == 'Exercises':  # ignore this case: Exercises 9 and 10
            if key_ind + 3 >= len(ocr_page_json):
                return False

            key_text_next1 = ocr_page_json[key_ind + 1][4]
            key_text_next2 = ocr_page_json[key_ind + 2][4]
            key_text_next3 = ocr_page_json[key_ind + 3][4]

            if key_text_next1.isdigit() and key_text_next2 == 'and' and key_text_next3.isdigit():
                return False

        elif primary_key == 'Example' or primary_key == 'EXAMPLE':  # only Example 2.1, Example 2-1, Example 2. is ok.
            key_text_next1 = ocr_page_json[key_ind + 1][4]

            # if key_ind + 2 < len(ocr_page_json):
            #     key_text_next2 = ocr_page_json[key_ind + 2][4]
            # else:
            #     key_text_next2 = 'end'
            #
            # if key_ind + 3 < len(ocr_page_json):
            #     key_text_next3 = ocr_page_json[key_ind + 3][4]
            # else:
            #     key_text_next3 = 'end'
            #
            # if key_text_next2 == '.' or (key_text_next1.isdigit() and key_text_next3.isdigit()):
            #     pass
            # else:
            #     return False

    else:
        if primary_key == 'Exercise':  # ignore this case: Exercise 33) to
            if key_ind + 3 >= len(ocr_page_json):
                return False

            key_text_next1 = ocr_page_json[key_ind + 1][4]
            key_text_next2 = ocr_page_json[key_ind + 2][4]
            key_text_next3 = ocr_page_json[key_ind + 3][4]

            if key_text_next1.isdigit() and key_text_next2 == ')' and key_text_next3 == 'to':
                return False

    return True


def check_start_key_ext(ocr_page_json, key_ind, char_width, page_type, page_rect, key_len_max):
    global primary_key, secondary_key
    f_match = False

    primary_key = ocr_page_json[key_ind][4]
    primary_key_rect = [func.get_rect_ocr_data(ocr_page_json, key_ind)]
    primary_key_width = primary_key_rect[0][2] - primary_key_rect[0][0]
    key_text_next = ''
    key_rect_next = None
    key_len = 0

    for key_len in range(1, key_len_max):
        if key_ind + key_len + 1 < len(ocr_page_json):
            primary_key += (ocr_page_json[key_ind + key_len][4])
            primary_key_rect.append(func.get_rect_ocr_data(ocr_page_json, key_ind + key_len))
            primary_key_width += (primary_key_rect[-1][2] - primary_key_rect[-1][0])

            key_text_next = ocr_page_json[key_ind + key_len + 1][4]
            key_rect_next = func.get_rect_ocr_data(ocr_page_json, key_ind + key_len + 1)
        else:
            return False

        # check key_start_list
        if not primary_key.isupper():
            break
        elif list_key_words_start.__contains__(primary_key):
            f_match = True
            break
        elif primary_key == 'REVIEWPROBLEMS':
            primary_key = 'REVIEW PROBLEMS'
            f_match = True
            break
        elif primary_key == 'DESIGNPROBLEMS':
            primary_key = 'DESIGN PROBLEMS'
            f_match = True
            break

    if not f_match:
        return False

    secondary_key = detect_secondary_key(ocr_page_json, key_ind + key_len + 1)
    # print '\r', secondary_key

    # check key position x1
    if page_type == 0:
        if primary_key_rect[0][0] > int((page_rect[0] + page_rect[2]) / 2.2):
            return False

    if float(primary_key_width) / len(primary_key) > char_width * 1.4:
        return True

    if key_text_next != '.' and key_text_next != ':' and not key_text_next.isdigit() and key_text_next != '*' and \
            (primary_key_rect[-1][1] - key_rect_next[1]) ** 2 + (primary_key_rect[-1][2] - key_rect_next[0]) ** 2 < 300:
        return False

    return True


def load_json_data(pdf_name, page_range, pdf_page_cnt):
    ocr_page_json_list = {}
    total_width = 0
    total_chars = 0
    for page_ind in page_range:
        sys.stdout.write('\rLoading image and json data: %d/%d' % (page_ind, pdf_page_cnt))
        sys.stdout.flush()

        ocr_page_json_org = func.load_ocr_data(pdf_name, page_ind, pdf_page_cnt)

        if ocr_page_json_org is not None:
            ocr_page_json = []
            for i in range(len(ocr_page_json_org)):

                # if ocr_page_json_org[i][4] == 'El':
                #     ocr_page_json_org[i][4] = 'E1'
                # elif ocr_page_json_org[i][4] == 'Ei':
                #     ocr_page_json_org[i][4] = 'E1'
                # elif ocr_page_json_org[i][4] == 'Cl':
                #     ocr_page_json_org[i][4] = 'C1'
                # elif ocr_page_json_org[i][4] == 'Ci':
                #     ocr_page_json_org[i][4] = 'C1'
                # elif ocr_page_json_org[i][4] == 'ci':
                #     ocr_page_json_org[i][4] = 'C1'
                # elif ocr_page_json_org[i][4] == 'Sl':
                #     ocr_page_json_org[i][4] = 'S1'
                # elif ocr_page_json_org[i][4] == 'Si':
                #     ocr_page_json_org[i][4] = 'S1'
                # elif ocr_page_json_org[i][4] == 'si':
                #     ocr_page_json_org[i][4] = 'S1'

                sub_key_text = ocr_page_json_org[i][4]
                sub_key_rect = func.get_rect_ocr_data(ocr_page_json_org, i)
                sub_key_width = sub_key_rect[2] - sub_key_rect[0]
                sub_key_height = sub_key_rect[3] - sub_key_rect[1]

                if list_key_remove.__contains__(sub_key_text):
                    continue
                elif sub_key_width < 3:
                    continue
                elif sub_key_height <= 5:
                    continue
                elif i > 0 and sub_key_height > 500:
                    ocr_page_json_org[i][3] = sub_key_rect[1] + 200

                ocr_page_json.append(ocr_page_json_org[i])

                if i > 0 and sub_key_text.isalpha() and sub_key_height < 60:
                    total_width += sub_key_width
                    total_chars += len(sub_key_text)

            ocr_page_json_list[page_ind] = ocr_page_json

    char_width = float(total_width) / total_chars
    print '\r'

    return ocr_page_json_list, char_width


def book_analyze(pdf_name, pdf_type, json_list, page_range, char_width, pdf_page_cnt, page_col_type_list,
                 rm_header, rm_footer, page_x1, page_y1, page_x2, page_y2):
    """
        key_type =  0 : end flag key
                    1 : start flag key
                    2 : end flag key (3.3 Chpater, 3.3.3 Chpater)
                    3 : stop flag key
                    4 : start flag key (2-column start same position)
    """
    global list_key_words_start, list_key_words_end, list_key_ignore_start, list_key_ignore_end, list_en_words
    global list_key_words_end_ignore, list_key_remove, primary_key, secondary_key, list_key_stop

    key_info_list = []
    key_dict = []
    book_info = {}
    page_type_list = {}
    f_ignore = False
    key_chk_text = ''
    key_type = 0
    draw_image = True

    write_image = True

    for page_ind in page_range:
        sys.stdout.write('\rAnalyzing data: %d/%d' % (page_ind, pdf_page_cnt))
        sys.stdout.flush()

        mark_img = func.load_img_data(pdf_name, page_ind, pdf_page_cnt)

        if mark_img is None or page_ind not in json_list:
            continue

        # ======================================= Step 2. Get Page information =========================================
        ocr_page_json = json_list[page_ind]
        page_col = page_col_type_list[page_ind]

        # if page_ind % 2 == 1:
        #     page_col = int(mark_img.shape[:2][1] * 0.49)
        # else:
        #     page_col = int(mark_img.shape[:2][1] * 0.52)

        if page_ind == 1910:
            page_col = int(mark_img.shape[:2][1] * 0.51)

        if pdf_type == 1 or (pdf_type == 2 and page_col is not None):
            if page_col is None:
                page_rect = func.get_rect_ocr_data(ocr_page_json, 0)
                page_type = func.check_columns(ocr_page_json, page_rect, get_min_value=True)
            else:
                page_type = page_col
        else:
            page_type = 0

        page_type_list[page_ind] = page_type
        page_rect, max_cross_min, max_cross_max, rect_half, page_height, page_width = \
            get_page_info(mark_img, ocr_page_json, page_type, page_ind, rm_header, rm_footer,
                          page_x1, page_y1, page_x2, page_y2)

        book_info[page_ind] = [page_rect, max_cross_min, max_cross_max, page_height, page_width]

        if write_image and draw_image:
            mark_img = func.img_draw_ocr_all_rect(mark_img, ocr_page_json)
            mark_img = func.img_draw_rect(mark_img, page_rect)

            if page_type > 0:
                rect_paragraph = [page_rect[0], max_cross_min, page_rect[2], max_cross_max]
                mark_img = func.img_draw_rect(mark_img, rect_half)
                mark_img = func.img_draw_rect(mark_img, rect_paragraph, (0, 125, 0), 2)

        # ========================================= Step 3. Search Keywords ============================================
        key_info_list_page = []
        ignore_words = 0

        for key_ind in range(1, len(ocr_page_json)):

            if ignore_words > 0:
                ignore_words -= 1
                continue

            key_rect = func.get_rect_ocr_data(ocr_page_json, key_ind)
            key_text = ocr_page_json[key_ind][4]
            primary_key = key_text
            secondary_key = ''
            key_big_rate = func.key_big(key_rect, key_text, char_width)

            if key_ind + 1 < len(ocr_page_json):
                key_text_next = ocr_page_json[key_ind + 1][4]
                key_rect_next = func.get_rect_ocr_data(ocr_page_json, key_ind + 1)
            else:
                key_text_next = ''
                key_rect_next = func.get_rect_ocr_data(ocr_page_json, key_ind)

            if key_ind + 2 < len(ocr_page_json):
                key_text_next2 = ocr_page_json[key_ind + 2][4]
            else:
                key_text_next2 = ''

            if not func.check_contain_rect(page_rect, key_rect):
                continue

            # if key_text == 'READING':
            #     print 'p'
            # if key_text == 'Finding' and key_text_next == 'the':
            #     print "p"

            # if key_ind == 290:
            #     print "d"

            key_type = 0

            # ----------- ignore when key is not alpha ------------
            if key_text.isalpha() and key_text[0].isupper():
                pass
            elif key_text.lower() == '3d' or key_text == 'www':
                pass
            elif key_text == 'volumes' and key_text_next == 'by':
                key_text = 'Volumes'
                ocr_page_json[key_ind][4] = 'Volumes'
                pass
            elif key_text == '\xe2\x80\x93':
                ocr_page_json[key_ind][4] = '-'
                continue
            else:
                continue

            # ------------ ignore when key is special --------------
            if key_text == 'Questions' and key_text_next == ',' and key_text_next2 == 'we':
                continue
            elif key_text == 'Example' and (key_text_next[:2] == 'TI' or key_text_next[:2] == 'XL'):
                continue
            elif key_text == 'Chapter' and key_ind > 0 and ocr_page_json[key_ind - 1][4].isdigit() and \
                    key_text_next.isdigit() and key_rect[1] - page_rect[1] < 120 and \
                    func.key_big(key_rect, 7, char_width) < 1.12:
                continue

            # --------- ignore when previous key is special ---------
            if len(key_info_list_page) > 0 and key_info_list_page[-1][3] == 'Hypothesis' and \
                    func.key_big(key_info_list_page[-1][4], 10, char_width) >= 1.94:
                continue
            elif key_text == 'Example' and len(key_info_list) > 0 and key_info_list[-1][3] == 'Hypothesis' and \
                    page_ind - key_info_list[-1][0] == 1 and \
                    func.key_big(key_info_list[-1][4], 10, char_width) >= 1.94:
                continue
            elif key_text == 'Example' and len(key_info_list) > 1 and key_info_list[-2][3] == 'Hypothesis' and \
                    page_type > 0 and page_ind - key_info_list[-2][0] == 1 and \
                    func.key_big(key_info_list[-2][4], 10, char_width) >= 1.94:
                continue

            if len(key_info_list_page) > 0 and key_info_list_page[-1][3] == 'Guided' and \
                    func.key_big(key_info_list_page[-1][4], 6, char_width) >= 4.0:
                continue
            elif key_text == 'Exercises' and len(key_info_list) > 0 and key_info_list[-1][3] == 'Guided' and \
                    page_ind - key_info_list[-1][0] == 1 and \
                    func.key_big(key_info_list[-1][4], 6, char_width) >= 4.0:
                continue

            # ---------------- find stop key --------------------
            # if list_key_stop.__contains__(key_text) and page_ind > 100 and \
            #         ((key_text.isupper() and key_big_rate >= 1.86) or key_big_rate >= 2.78):
            #     primary_key = key_text
            #     top_margin = 10
            #     left_margin = 30
            #     key_type = 3

            # ------- find start point (new line: 3.3 EXERCISES) ----
            if func.check_key(ocr_page_json, key_ind, 'nd.dU', page_type, rect_half) and (
                    key_text == 'EXERCISES' or key_text == 'PROBLEMS'):
                top_margin = 13
                left_margin = 13
                key_type = 4
                secondary_key = ocr_page_json[key_ind - 3][4] + '.' + ocr_page_json[key_ind - 1][4]

            # ------- find end point (new line: *3.3.2 Chapter) ----
            elif func.check_key(ocr_page_json, key_ind, 'n*d.d.du', page_type, rect_half):
                top_margin = 13
                left_margin = 13
                key_type = 2

            # ------- find end point (new line: 3.3.2 Chapter) ----
            elif func.check_key(ocr_page_json, key_ind, 'nd.d.du', page_type, rect_half) or \
                    func.check_key(ocr_page_json, key_ind, 'nd-d-du', page_type, rect_half):
                top_margin = 13
                left_margin = 13
                key_type = 2

            # ------- find end point (new line: *3.3 Chapter) ----
            elif func.check_key(ocr_page_json, key_ind, 'n*d.du', page_type, rect_half):
                top_margin = 13
                left_margin = 13
                key_type = 2

            # ------ find end point (new line: 3.3 Chapter) ----------
            elif func.check_key(ocr_page_json, key_ind, 'nd.du', page_type, rect_half) or \
                    func.check_key(ocr_page_json, key_ind, 'nd.Tu', page_type, rect_half) or \
                    func.check_key(ocr_page_json, key_ind, 'nd.d*u', page_type, rect_half):
                # if key_text == 'An' or key_text == 'In':
                #     continue
                # elif key_text == 'How' and not key_text_next[0].isupper():
                #     continue

                if ocr_page_json[key_ind - 1][4] == '*':
                    left_margin = 6
                else:
                    left_margin = 13

                key_type = 2
                top_margin = 17

            # ------ find end point (new line: 3-3 Chapter) ----------
            elif func.check_key(ocr_page_json, key_ind, 'nd-du', page_type, rect_half) or \
                    func.check_key(ocr_page_json, key_ind, 'nd%du', page_type, rect_half):
                num1 = int(ocr_page_json[key_ind - 3][4])
                num2 = int(ocr_page_json[key_ind - 1][4])
                if key_text == 'An':
                    continue
                elif key_text == 'How' and not key_text_next[0].isupper():
                    continue
                elif 40 < num1 < num2:
                    continue

                top_margin = 17
                left_margin = 13
                key_type = 2

            # ------ find end point (new line: 3 Chapter) ----------
            elif func.check_key(ocr_page_json, key_ind, 'ndu', page_type, rect_half) and \
                    key_rect[3] - key_rect[1] > 30 and key_big_rate >= 1.5:
                top_margin = 25
                left_margin = 20
                key_type = 2

            # --------------- ignore end_ignore keywords -----------
            elif list_key_words_end_ignore.__contains__(key_text):
                continue

            elif list_key_words_end_ignore.__contains__(key_text + ' ' + key_text_next) and \
                    -6 < key_rect_next[0] - key_rect[2] < 40 and abs(key_rect[1] - key_rect_next[1]) < 10:
                ignore_words = 1
                keys_width = key_rect[2] - key_rect[0] + key_rect_next[2] - key_rect_next[0]
                key_width = float(keys_width) / (len(key_text) + len(key_text_next))
                if key_text == 'Problems' and key_text_next == 'Plus' and key_width > char_width * 2.5:
                    top_margin = 17
                    left_margin = 13
                else:
                    continue

            elif list_key_words_end_ignore.__contains__(key_text + ' ' + key_text_next) and \
                    abs(key_rect[3] - key_rect_next[1]) < 10 and abs(key_rect[0] - key_rect_next[0]) < 10:
                ignore_words = 1
                continue

            elif list_key_words_end_ignore.__contains__(key_text + ' ' + key_text_next + ' ' + key_text_next2):
                ignore_words = 2
                continue

            # ------------ find start point (P RO BML EMS) -----------------
            elif check_start_key_ext(ocr_page_json, key_ind, char_width, page_type, page_rect, 9):
                key_type = 1
                top_margin = 7
                left_margin = 20
                if page_type > 0:
                    if primary_key == 'REVIEW PROBLEMS' and key_rect[1] - page_rect[1] < 30:
                        if int((page_rect[0] + page_type) / 2) < key_rect[0] < page_type:
                            continue
                        elif int((page_rect[2] + page_type) / 2) < key_rect[0] < page_rect[2]:
                            continue

            # ------------- find start point with 4 words ---------
            elif check_start_key(ocr_page_json, key_ind, char_width, page_type, page_rect, 4, rect_half):
                ignore_words = 3
                key_type = 1
                top_margin = 7
                if primary_key.isupper():
                    left_margin = 20
                else:
                    left_margin = 27

            # ------------- find start point with 3 words ---------
            elif check_start_key(ocr_page_json, key_ind, char_width, page_type, page_rect, 3, rect_half):
                ignore_words = 2
                key_type = 1
                top_margin = 7
                if primary_key.isupper():
                    left_margin = 20
                else:
                    left_margin = 27

            # ------------- find start point with 2 words ---------
            elif check_start_key(ocr_page_json, key_ind, char_width, page_type, page_rect, 2, rect_half):
                ignore_words = 1
                key_type = 1
                top_margin = 7
                if primary_key.isupper():
                    left_margin = 20
                else:
                    left_margin = 30

            # ------------------ find start point -----------------
            elif check_start_key(ocr_page_json, key_ind, char_width, page_type, page_rect, 1, rect_half):
                key_type = 1
                if key_text.isupper() or (key_big_rate > 1.45 and key_text == 'Exercises'):
                    top_margin = 7
                    left_margin = 20
                else:
                    top_margin = 8
                    left_margin = 30

                # if secondary_key != '':
                #     continue

            # --------------- find end point (keywords) -----------
            elif list_key_words_end.__contains__(key_text):
                if key_text == 'Chapter' and page_rect[3] - key_rect[3] < 25:
                    continue

                primary_key = key_text
                if page_type == 0:
                    if key_chk_text == 'Questions' and (key_text == 'Theorem' or key_text == 'Proof'):
                        continue

                    if key_text.isupper():
                        top_margin = 2
                        left_margin = 15
                    else:
                        if key_text == 'Chapter':
                            top_margin = 7
                        else:
                            top_margin = 4
                        left_margin = 35

                    # if key_rect[0] > int((page_rect[0] + page_rect[2]) / 2.2):  # check key position x1
                    #     if key_big_rate > 5 and func.get_contain_counts(key_rect, ocr_page_json) == 0:
                    #         pass
                    #     elif key_big_rate > 1.36 and key_text == 'Solution' and page_rect[2] - key_rect[2] < 170:
                    #         left_margin = 20
                    #     else:
                    #         continue
                    # else:
                    #     pass
                else:
                    if key_big_rate > 6:
                        pass
                    elif key_text.isupper():
                        pass
                    elif int(page_type * 0.5) < key_rect[0] < page_type:
                        continue
                    elif int(page_type * 1.4) < key_rect[0]:
                        continue

                    top_margin = 7
                    left_margin = 30

            # ---------- find end point (first char is upper) ------
            elif 65 <= ord(key_text[0]) <= 90 or key_text[0] == '3':
                top_margin = 12
                left_margin = 45
                if page_type == 0:
                    if func.get_overlap_counts(key_rect, ocr_page_json) > 0:
                        continue
                    elif key_rect[0] > int((page_rect[0] + page_rect[2]) / 2.12):  # check key position x1
                        if key_big_rate > 2.5:
                            pass
                        else:
                            continue
                    elif len(key_text) > 4:
                        if key_big_rate > 1.6:  # 1.3
                            pass
                        else:
                            continue
                    elif len(key_text) > 2:
                        if key_big_rate > 1.8:  # 1.5
                            pass
                        else:
                            continue
                    elif len(key_text) == 2 and key_text.isupper() and key_big_rate > 1.9:
                        left_margin = 40
                    else:
                        continue
                else:
                    if key_text.isupper() and key_big_rate > 1.58:
                        left_margin = 20
                        top_margin = 10
                        pass
                    elif len(key_text) > 4 and key_big_rate > 1.38:
                        pass
                    else:
                        continue

            else:
                continue

            ret = func.anal_key(ocr_page_json=ocr_page_json,
                                page_ind=page_ind,
                                key_type=key_type,
                                key_ind=key_ind,
                                page_type=page_type,
                                img=mark_img,
                                t_margin=top_margin,
                                l_margin=left_margin,
                                img_draw=(draw_image and write_image))

            if ret is None:
                continue

            # -- ignore when key isn't included in en dictionary ---
            if len(primary_key.split()) == 1:
                if not primary_key.lower() in key_dict:
                    if list_en_words.__contains__(primary_key.lower()):
                        key_dict.append(primary_key.lower())
                    else:
                        continue

            # ------ ignore when key is above the page_rect and same line with previous key -------
            if key_rect[1] + key_rect[3] < 2 * page_rect[1] and key_type != 3:
                if page_rect[1] > 160 and key_rect[3] - key_rect[1] > 50:
                    pass
                if page_rect[1] > 110 and key_rect[3] - key_rect[1] > 36 and key_big_rate > 2:
                    pass
                else:
                    continue

            elif func.check_prev_key_same_line(key_info_list_page, key_rect):
                t_val = key_info_list_page[-1][4][0]
                if page_type > 0 and (key_rect[0] < page_type < t_val or t_val < page_type < key_rect[0]):
                    pass
                else:
                    if key_rect[0] > key_info_list_page[-1][4][0]:
                        continue
                    else:
                        key_info_list_page[-1][2] = key_type
                        key_info_list_page[-1][3] = primary_key
                        key_info_list_page[-1][4] = key_rect
                        key_info_list_page[-1][5] = secondary_key
                        continue

            key_info_list_page.append([ret[0], ret[1], ret[2], primary_key, ret[3], secondary_key])

            if page_type > 0:
                if key_text == 'REFERENCES':
                    if key_rect[0] > int((page_rect[2] - page_rect[0]) / 6) + page_rect[0]:
                        new_ret = [ret[0], int(ret[1] + page_height), ret[2], primary_key, ret[3]]
                        key_info_list_page.append(new_ret)
                elif key_text == 'Chapter':
                    half = int((page_rect[2] + page_rect[0]) / 2)
                    if key_rect[0] > half and ret[1] > page_height:
                        new_ret = [ret[0], int(ret[1] - page_height), ret[2], primary_key,
                                   [ret[3][0] - half, ret[3][1], ret[3][2] - half, ret[3][3]]]
                        key_info_list_page.append(new_ret)

            if key_type == 3:
                break

        # ============================= Step 4. Arrangement and Collect all Keywords ===================================
        key_info_list_page = sorted(key_info_list_page, key=lambda l: l[1])

        # ------------------ ignore some keys --------------
        for i in range(len(key_info_list_page)):
            key_chk_rect = key_info_list_page[i][4]
            key_chk_text = key_info_list_page[i][3]
            key_chk_type = key_info_list_page[i][2]

            if f_ignore:
                if list_key_ignore_end.__contains__(key_chk_text) or list_key_ignore_start.__contains__(key_chk_text):
                    f_ignore = False
                elif key_chk_type == 2:
                    # f_ignore = False
                    if func.key_big(key_chk_rect, key_chk_text, char_width) > 1.6:  # min: 1.39
                        f_ignore = False
                    if func.key_big(key_chk_rect, key_chk_text, char_width) > 1.4 and \
                            key_chk_rect[3] - key_chk_rect[1] > 35:  # min: 1.23
                        f_ignore = False
                    elif key_chk_text == 'A':
                        if func.key_big(key_chk_rect, 1, char_width) > 1.13:
                            f_ignore = False
                        elif func.key_big(key_chk_rect, 1, char_width) > 0.89 and key_chk_rect[3]-key_chk_rect[1] > 25:
                            f_ignore = False
                elif len(key_chk_text) >= 2:
                    if func.key_big(key_chk_rect, key_chk_text, char_width) > 1.7:  # min:1.7
                        f_ignore = False

            if not f_ignore:
                key_info_list.append(key_info_list_page[i])
                if draw_image and write_image:
                    if key_chk_type == 1 or key_chk_type == 4:
                        mark_img = func.img_draw_rect(mark_img, key_chk_rect, (255, 0, 0), width=3)
                    else:
                        mark_img = func.img_draw_rect(mark_img, key_chk_rect, (0, 125, 0), width=3)

            if list_key_ignore_start.__contains__(key_chk_text):
                f_ignore = True

        # ======================================= Step 5. Output result images =========================================
        if write_image:
            if len(page_range) == 1:
                cv2.imwrite('temp.jpg', mark_img)

            if not os.path.isdir('../temp'):
                os.makedirs('../temp')

            cv2.imwrite('../temp/temp' + str(page_ind) + '.jpg', mark_img)

        if key_type == 3:
            break

    print '\r'
    return key_info_list, book_info, page_type_list


if __name__ == '__main__':
    class_ocr = GoogleOCR()
    class_extract = ExtractData()

    # ------------------------------ load config -----------------------------------
    list_key_words_start = []
    list_key_words_end = []
    list_key_ignore_start = []
    list_key_ignore_end = []
    list_key_words_end_ignore = []
    list_key_remove = []
    list_en_words = []
    list_key_stop = []
    primary_key = ''
    secondary_key = ''
    page_x1_val = None
    page_y1_val = None
    page_x2_val = None
    page_y2_val = None
    thread_state = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    load_config()

    # book_list = range(194, 201)
    # book_list = [22, 23, 24, 25]
    book_list = [380]

    remove_header_pixel = 50
    remove_footer_pixel = 0

    # page_x1_val = 10
    page_y1_val = 105
    # page_x2_val = 1030
    # page_y2_val = 1280

    same_line_start = True
    same_line_end = True

    for book_ind in book_list:
        # pdf_name = 'book11'
        pdf_file = 'book' + str(book_ind)
        print pdf_file

        # -------------------------- Convert pdf to jpg --------------------------------
        # conv_pdf2jpg(pdf_file)

        # ------------------------- Get OCR text for jpg -------------------------------
        # conv_jpg2ocr(pdf_file, f_thread=True)

        # ----------------------------- Analyzing Book ---------------------------------
        time1 = time.time()
        _, _, join_list_pages = func.get_file_list('../jpg/' + pdf_file)
        pdf_pages = len(join_list_pages)

        page_range_out = range(1, pdf_pages + 1)
        # page_range_out = range(695, 702)
        # page_range_out = [695]

        json_list_out, char_width_out = load_json_data(pdf_file, page_range_out, pdf_pages)

        pdf_file_type, page_col_type_out = get_pdf_type(json_list_out)
        pdf_file_type = 0
        # char_width_out = 15.5

        print "\r\tThe type of pdf is:", pdf_file_type
        print "\tchar width =", char_width_out

        r1, r2, r3 = book_analyze(pdf_file, pdf_file_type, json_list_out, page_range_out, char_width_out, pdf_pages,
                                  page_col_type_out, remove_header_pixel, remove_footer_pixel,
                                  page_x1_val, page_y1_val, page_x2_val, page_y2_val)

        # for i in range(1, len(r1)):
        #     r1.pop(1)
        # r1.pop(1)

        ret_analyze = [r1, r2, r3]

        class_extract.extract_data(pdf_file, json_list_out, ret_analyze, pdf_pages, json_list_out, page_range_out,
                                   same_line_start, same_line_end)

        print '\r', int(time.time() - time1)
