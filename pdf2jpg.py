
import sys
import os


def pdf2jpg_ppm(in_name, out_name, page=1, max_pdf_page=10):
    """
        convert the pdf file to jpg file
        page: None  => whole page
        page: 1~    => page number
    """

    command = "pdftoppm \"%s\" \"%s\" -jpeg" % (in_name, out_name)
    os.system(command)
    page_cnt = 0

    for file in os.listdir(os.curdir):
        if file.endswith(".jpg") and file.startswith(out_name):
            p_ind = int(file[len(out_name)+1:len(file)-4])

            if page == 0 and p_ind < max_pdf_page:
                page_cnt += 1
                os.rename(file, out_name + str(p_ind - 1) + '.jpg')

            elif p_ind == page:
                os.rename(file, out_name + '0.jpg')
                page_cnt = 1

            else:
                os.remove(file)

    return page_cnt


if __name__ == '__main__':
    in_arg = ['doc1 white.pdf',           # input pdf file name
              '10',           # output jpg file name
              '0',          # resolution
              '0',          # page
              '10']         # max pdf page

    for arg_ind in range(len(sys.argv) - 1):
        in_arg[arg_ind] = sys.argv[arg_ind + 1]

    in_name = in_arg[0]
    out_name = in_arg[1]
    resol = int(in_arg[2])
    page = int(in_arg[3])
    max_page = int(in_arg[4])
    # convert_pages = pdf2jpg(in_name=in_name, out_name=out_name, resolution=resol, page=page, max_pdf_page=max_page)
    convert_pages = pdf2jpg_ppm(in_name=in_name, out_name=out_name, page=page, max_pdf_page=max_page)

    exit(convert_pages)
